<?php

/**
 * Add to Cart popup modal template, it wll be displayed on shop, category, and products pages. 
 * @author   Tyche Softwares
 * @package Abandoned-Cart-Pro-for-WooCommerce/Frontend/ATC-Template
 * @since 6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$page_id = get_the_ID();
$template_settings = wcap_get_popup_template_for_page( $page_id, 'exit_intent' );

?>
<div class = "wcap_container" id = "wcap_popup_main_div">
    <div class = "wcap_popup_wrapper">
        <div class = "wcap_popup_content">
            <div class = "wcap_popup_heading_container">
                <div class = "wcap_popup_icon_container" >
                    <span class = "wcap_popup_icon" v-model = "wcap_ei_button" >
                        <span class = "wcap_popup_plus_sign" v-bind:style = "wcap_ei_button">
                        </span>
                    </span>
                </div>
                <div class = "wcap_popup_text_container">
                    <h2 class = "wcap_popup_heading" v-model = "wcap_quick_ck_heading" v-bind:style = "wcap_ei_popup_heading" >{{wcap_quick_ck_heading}}</h2>
                    <div class = "wcap_popup_text" v-bind:style = "wcap_ei_popup_text" v-model = "wcap_quick_ck_text" >{{wcap_quick_ck_text}}
                    </div>
                </div>
            </div>
            <div class = "wcap_popup_form">
                <form action = "" name = "wcap_modal_form">
                    <?php // do_action( 'wcap_atc_after_email_field' ); ?>

                    <button class = "wcap_popup_button" v-bind:style = "wcap_ei_button" v-model = "wcap_quick_ck_button" id="wcap_ei_quick_ck_button">{{wcap_quick_ck_button}}
                    </button>

                    <div class="clear"></div>

                    <?php if ( 'on' === $template_settings['wcap_atc_auto_apply_coupon_enabled'] && '' !== $template_settings['wcap_atc_coupon_type'] ) { ?>
                        <div id="wcap_coupon_auto_applied" class="woocommerce-info">
                            <p class="wcap_atc_coupon_auto_applied_msg" > {{wcap_atc_coupon_applied_msg}} </p>
                        </div>
                    <?php } ?>
                </form>
            </div>
            <div class = "wcap_popup_close" id="wcap_ei_popup_close"></div>
        </div>
    </div>
</div>