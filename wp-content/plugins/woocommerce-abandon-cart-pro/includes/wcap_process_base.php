<?php

class Wcap_Process_Base {

    /**
     * Construct.
     */
	public function __construct() {

		$wcap_auto_cron = get_option( 'wcap_use_auto_cron' );
		if ( isset( $wcap_auto_cron ) && $wcap_auto_cron != false && '' != $wcap_auto_cron ) {
			// Hook into that action that'll fire based on the cron frequency.
			add_action( 'woocommerce_ac_send_email_action', array( &$this, 'wcap_process_handler' ), 11 );
		}

		$wcap_email_reports_frequency = get_option( 'wcap_email_reports_frequency' );
		if ( '' !== $wcap_email_reports_frequency ) {
			// Hook into that action that'll fire based on the cron frequency.
			add_action( 'wcap_email_reports_frequency_action', array( &$this, 'wcap_email_reports_frequency_action_handler' ), 11 );
		}
	}

    /**
     * Execute functions to send reminders.
     */
	public function wcap_process_handler() {

		// Check if reminders are enabled.
		$reminders_list = wcap_get_enabled_reminders();

		if ( is_array( $reminders_list ) && count( $reminders_list ) > 0 ) {
			foreach ( $reminders_list as $reminder_type ) {
				switch ( $reminder_type ) {
					case 'emails':
						$active_count = Wcap_Connectors_Common::wcap_get_active_connectors_count();
						$send_emails  = $active_count > 0 ? false : true;
						$send_emails  = apply_filters( 'wcap_send_reminder_emails', $send_emails );
						if ( $send_emails ) {
							Wcap_Send_Email_Using_Cron::wcap_abandoned_cart_send_email_notification();
						}
						break;
					case 'sms':
						Wcap_Send_Email_Using_Cron::wcap_send_sms_notifications();
						break;
					case 'fb':
						WCAP_FB_Recovery::wcap_fb_cron();
						break;
				}
			}
		}

	}

	/**
	 * Execute functions to send reminders.
	 */
	public function wcap_email_reports_frequency_action_handler() {
		if ( ! class_exists( 'Wcap_Dashboard_Report' ) ) {
			include_once 'classes/class_wcap_dashboard_report.php';
		}

		$from_email = get_option( 'admin_email' );
		$blogname   = get_option( 'blogname' );
		$blogname   = str_replace( "&#039;", "'", $blogname );

		$wcap_email_reports_frequency = get_option( 'wcap_email_reports_frequency' );
		if ( 'weekly' === $wcap_email_reports_frequency ) {
			$start_date   = date( 'Y-m-d', strtotime( 'last monday' ) ); // phpcs:ignore
			$end_date     = date( 'Y-m-d', strtotime( '+6 days', strtotime( $start_date ) ) ); // phpcs:ignore
			$date_range   = 'other';
			$subject_line = 'Weekly';
			$summary_text = 'week';
		} elseif ( 'monthly' === $wcap_email_reports_frequency ) {
			$start_date   = date( 'Y-m-d', strtotime( 'first day of previous month' ) ); // phpcs:ignore
			$end_date     = date( 'Y-m-d', strtotime( 'last day of previous month' ) ); // phpcs:ignore
			$date_range   = 'other';
			$summary_text = 'month';
			$subject_line = 'Monthly';
		}
		$orders                = new Wcap_Dashboard_Report();
		$wcap_recovered_amount = $orders->get_this_month_amount_reports( 'recover', $date_range, $start_date, $end_date );
		/* translators: %1$s is replaced recovered amount, %2$s: report frequency */
		$wcap_recovered_text = sprintf( __( 'You recovered', 'woocommerce-ac' ) . ' %1$s ' . __( 'in revenue this %2$s!', 'woocommerce-ac' ), wc_price( $wcap_recovered_amount ), $summary_text );
		$wcap_recovered_text = apply_filters( 'wcap_email_report_heading_text', $wcap_recovered_text, wc_price( $wcap_recovered_amount ) );

		$wcap_email_abandoned_cart_count = $orders->get_this_month_number_reports( 'abandoned', $date_range, $start_date, $end_date );
		$wcap_email_abandoned_cart_text  = __( 'carts abandoned', 'woocommerce-ac' );
		$wcap_email_abandoned_cart_text  = apply_filters( 'wcap_email_report_abandoned_cart_text', $wcap_email_abandoned_cart_text );
		$wcap_email_recovered_cart_count = $orders->get_this_month_number_reports( 'recover', $date_range, $start_date, $end_date );
		$wcap_email_recovered_cart_text  = __( 'carts recovered', 'woocommerce-ac' );
		$wcap_email_recovered_cart_text  = apply_filters( 'wcap_email_report_recovered_cart_text', $wcap_email_recovered_cart_text );

		$wcap_sales_heading_text      = __( 'Sales figures', 'woocommerce-ac' );
		$wcap_sales_heading_text      = apply_filters( 'wcap_email_report_sales_heading_text', $wcap_sales_heading_text );
		$wcap_stats_of_abandoned_cart = $orders->get_adv_stats( $date_range, $start_date, $end_date );
		$wcap_text_of_abandoned_cart  = __( 'worth carts abandoned', 'woocommerce-ac' );
		$wcap_text_of_abandoned_cart  = apply_filters( 'wcap_email_report_worth_abandoned_cart_text', $wcap_text_of_abandoned_cart );
		$wcap_worth_of_abandoned_cart = wc_price( $wcap_stats_of_abandoned_cart['abandoned_amount'] );

		$wcap_text_of_recovered_cart  = __( 'worth carts recovered', 'woocommerce-ac' );
		$wcap_text_of_recovered_cart  = apply_filters( 'wcap_email_report_worth_recovered_cart_text', $wcap_text_of_abandoned_cart );
		$wcap_worth_of_recovered_cart = wc_price( $wcap_stats_of_abandoned_cart['recovered_count'] );

		$wcap_campaign_title       = __( 'Campaign Engagement', 'woocommerce-ac' );
		$wcap_campaign_title       = apply_filters( 'wcap_email_report_campaign_title', $wcap_campaign_title );
		$wcap_remider_text         = __( 'Reminders Sent', 'woocommerce-ac' );
		$wcap_remider_text         = apply_filters( 'wcap_email_report_remider_sent_text', $wcap_remider_text );
		$wcap_email_sent_count     = $orders->wcap_get_email_report( 'total_sent', $date_range, $start_date, $end_date );
		$wcap_open_text            = __( 'Open', 'woocommerce-ac' );
		$wcap_open_text            = apply_filters( 'wcap_email_report_open_text', $wcap_open_text );
		$wcap_email_opened_count   = $orders->wcap_get_email_report( 'total_opened', $date_range, $start_date, $end_date );
		$wcap_clicks_text          = __( 'Clicks', 'woocommerce-ac' );
		$wcap_clicks_text          = apply_filters( 'wcap_email_report_clicks_text', $wcap_clicks_text );
		$wcap_email_clicked_count  = $orders->wcap_get_email_report( 'total_clicked', $date_range, $start_date, $end_date );
		$wcap_conversions_text     = __( 'Conversions', 'woocommerce-ac' );
		$wcap_conversions_text     = apply_filters( 'wcap_email_report_conversions_text', $wcap_conversions_text );
		$wcap_abandoned_cart_count = $wcap_stats_of_abandoned_cart['abandoned_count'];
		$wcap_recovered_cart_count = $wcap_stats_of_abandoned_cart['recovered_count'];

		$wcap_dashboard_text = __( 'View Dashboard', 'woocommerce-ac' );
		$wcap_dashboard_text = apply_filters( 'wcap_email_report_dashboard_button_text', $wcap_dashboard_text );

		$wcap_email_reports_emails_list = get_option( 'wcap_email_reports_emails_list' );
		$wcap_email_reports_emails_list = implode( ',', explode( PHP_EOL, $wcap_email_reports_emails_list ) );

		$wcap_email_summary_title = sprintf( '%s ' . __( 'Account Summary for', 'woocommerce-ac' ) . ' %s', $subject_line, $blogname );
		$wcap_email_summary_title = apply_filters( 'wcap_email_report_summary_title', $wcap_email_summary_title, $subject_line, $blogname );
		/* translators: %1$s is replaced with type of frequency for reports, %2$s: report frequency, %3$s: report frequency */
		$wcap_email_summary_text   = sprintf( __( 'Below is the', 'woocommerce-ac' ) . ' %1$s ' . __( 'summary of the revenue recovered during this %2$s using Abandoned Cart Pro for WooCommerce, as well as the percentage change from last %3$s.', 'woocommerce-ac' ), strtolower( $subject_line ), $summary_text, $summary_text );
		$wcap_email_summary_text   = apply_filters( 'wcap_email_report_summary_text', $wcap_email_summary_text, strtolower( $subject_line ) );
		$wcap_date_formatted_range = date( 'd F', strtotime( $start_date ) ) . ' - ' . date( 'd F Y', strtotime( $end_date ) ); // phpcs:ignore

		$wcap_footer_text = sprintf( __( '&copy; 2012-%1$s Tyche Softwares, All Rights Reserved.', 'woocommerce-ac' ), date( 'Y' ) ); // phpcs:ignore
		$wcap_footer_text = apply_filters( 'wcap_email_footer_text', $wcap_footer_text );

		$wcap_get_last_report_data      = get_option( 'wcap_last_email_report_data' );
		$wcap_abandoned_cart_percentage = '';
		$wcap_recovered_cart_percentage = '';
		if ( $wcap_get_last_report_data ) {
			if ( isset( $wcap_get_last_report_data[ $wcap_email_reports_frequency ] ) ) {
				$wcap_last_report_count = $wcap_get_last_report_data[ $wcap_email_reports_frequency ];
				if ( $wcap_last_report_count['abandoned_count'] > 0 ) {
					$wcap_abandoned_cart_percentage = ( ( $wcap_email_abandoned_cart_count - $wcap_last_report_count['abandoned_count'] ) / $wcap_last_report_count['abandoned_count'] ) * 100;
				}
				if ( $wcap_last_report_count['recovered_count'] > 0 ) {
					$wcap_recovered_cart_percentage = ( ( $wcap_email_recovered_cart_count - $wcap_last_report_count['recovered_count'] ) / $wcap_last_report_count['recovered_count'] ) * 100;
				}
			}
		}

		$wcap_last_email_report_data_value = array(
			$wcap_email_reports_frequency => array(
				'abandoned_count' => $wcap_email_abandoned_cart_count,
				'recovered_count' => $wcap_email_recovered_cart_count,
			),
		);
		update_option( 'wcap_last_email_report_data', $wcap_last_email_report_data_value );

		$email_output = wc_get_template_html(
			'template-email-reports.php',
			array(
				'base_url'                => WCAP_PLUGIN_URL . '/assets/images',
				'dashboard_url'           => admin_url( 'admin.php?page=woocommerce_ac_page' ),
				'recovered_amount_text'   => $wcap_recovered_text,
				'email_summary_title'     => $wcap_email_summary_title,
				'email_summary_text'      => $wcap_email_summary_text,
				'date_formatted_range'    => $wcap_date_formatted_range,
				'abandoned_cart_count'    => $wcap_email_abandoned_cart_count,
				'abandoned_cart_text'     => $wcap_email_abandoned_cart_text,
				'recovered_cart_count'    => $wcap_email_recovered_cart_count,
				'recovered_cart_text'     => $wcap_email_recovered_cart_text,
				'sales_heading_text'      => $wcap_sales_heading_text,
				'text_of_abandoned_cart'  => $wcap_text_of_abandoned_cart,
				'worth_of_abandoned_cart' => $wcap_worth_of_abandoned_cart,
				'text_of_recovered_cart'  => $wcap_text_of_recovered_cart,
				'worth_of_recovered_cart' => $wcap_worth_of_recovered_cart,
				'campaign_title'          => $wcap_campaign_title,
				'remider_text'            => $wcap_remider_text,
				'email_sent_count'        => $wcap_email_sent_count,
				'open_text'               => $wcap_open_text,
				'email_opened_count'      => $wcap_email_opened_count,
				'clicks_text'             => $wcap_clicks_text,
				'email_clicked_count'     => $wcap_email_clicked_count,
				'conversions_text'        => $wcap_conversions_text,
				'conversions_count'       => $wcap_recovered_cart_count,
				'dashboard_text'          => $wcap_dashboard_text,
				'abandoned_cart_percent'  => $wcap_abandoned_cart_percentage,
				'recovered_cart_percent'  => $wcap_recovered_cart_percentage,
				'footer_text'             => $wcap_footer_text,
			),
			'woocommerce-abandon-cart-pro/',
			WCAP_PLUGIN_PATH . '/includes/template/emails/'
		);

		$subject = sprintf( '%s ' . __( 'summary for', 'woocommerce-ac' ) . ' %s', $subject_line, $blogname );
		$subject = apply_filters( 'wcap_email_report_subject', $subject, $subject_line, $blogname );

		$headers  = "From: " . $blogname . " <" . $from_email . ">" . "\r\n"; // phpcs:ignore
		$headers .= "Content-Type: text/html" . "\r\n"; // phpcs:ignore

		Wcap_Common::wcap_add_wp_mail_header();
		wp_mail( $wcap_email_reports_emails_list, $subject, stripslashes( $email_output ), $headers );
		Wcap_Common::wcap_remove_wc_mail_header();
	}
}
new Wcap_Process_Base();
