<?php
/**
 * It will display the email template listing.
 *
 * @author   Tyche Softwares
 * @package Abandoned-Cart-Pro-for-WooCommerce/Admin/Template
 * @since 5.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Wcap_ATC_Templates_List' ) ) {
	/**
	 * It will display the email template listing, also it will add, update & delete the email template in the database.
	 *
	 * @since 5.0
	 */
	class Wcap_ATC_Templates_List {

		/**
		 * It will display the email template listing, also it will add, update & delete the email template in the database.
		 *
		 * @param string $wcap_action Action name.
		 * @param string $wcap_section Section Name.
		 * @param string $wcap_mode Mode name.
		 * @globals mixed $wpdb
		 * @globals mixed $woocommerce
		 * @since 5.0
		 */
		public static function wcap_display_atc_template_list( $wcap_action, $wcap_section, $wcap_mode ) {
			global $woocommerce, $wpdb;
			?>
			<p>
				<?php esc_html_e( 'Add different Add to Cart popup templates for different pages to maximize the possibility of collecting email addresses from users.', 'woocommerce-ac' ); ?>
			</p>
			<?php
			$wcap_action  = isset( $_GET['action'] ) ? sanitize_text_field( wp_unslash( $_GET['action'] ) ) : ''; // phpcs:ignore WordPress.Security.NonceVerification
			$wcap_section = isset( $_GET['wcap_section'] ) ? sanitize_text_field( wp_unslash( $_GET['wcap_section'] ) ) : ''; // phpcs:ignore WordPress.Security.NonceVerification
			$mode         = isset( $_GET['mode'] ) ? sanitize_text_field( wp_unslash( $_GET['mode'] ) ) : ''; // phpcs:ignore WordPress.Security.NonceVerification

			if ( isset( $_POST['atc_settings_frm'] ) && in_array( $_POST['atc_settings_frm'], array( 'save', 'update' ), true ) ) { // phpcs:ignore WordPress.Security.NonceVerification
				$update_id = Wcap_Add_Cart_Popup_Modal::wcap_add_to_cart_popup_save_settings();
				Wcap_Display_Notices::wcap_add_to_cart_popup_save_success();
			}

			if ( 'emailsettings' === $wcap_action && 'wcap_atc_settings' === $wcap_section && 'deleteatctemplate' === $wcap_mode ) { // phpcs:ignore WordPress.Security.NonceVerification
				$id = isset( $_GET['id'] ) && '' !== $_GET['id'] ? sanitize_text_field( wp_unslash( $_GET['id'] ) ) : 0; // phpcs:ignore WordPress.Security.NonceVerification
				if ( $id > 0 ) {
					$wpdb->delete( //phpcs:ignore
						WCAP_ATC_RULES_TABLE,
						array(
							'id' => $id,
						)
					);
					Wcap_Display_Notices::wcap_display_notice( 'wcap_template_deleted' );
				}
			}

			if ( isset( $_POST['atc_settings_frm'] ) && '' !== $_POST['atc_settings_frm'] && ( isset( $update_id ) && $update_id > 0 ) ) { // phpcs:ignore WordPress.Security.NonceVerification
				Wcap_Display_Notices::wcap_template_save_success();
			} elseif ( ( isset( $update_id ) && '' === $update_id ) && isset( $_POST['ac_settings_frm'] ) && '' !== $_POST['ac_settings_frm'] ) { // phpcs:ignore WordPress.Security.NonceVerification
				Wcap_Display_Notices::wcap_template_save_error();
			}

			?>
			<div>
				<p>
					<a cursor: pointer; href="<?php echo 'admin.php?page=woocommerce_ac_page&action=emailsettings&wcap_section=wcap_atc_settings&mode=addnewtemplate'; ?>" class="button-secondary"><?php esc_html_e( 'Add New Template', 'woocommerce-ac' ); ?></a>
				</p>
				<?php

				// From here you can do whatever you want with the data from the $result link.
				$wcap_atc_template_list = new Wcap_ATC_Templates_Table();
				$wcap_atc_template_list->wcap_templates_prepare_items();
				?>
				<div class="wrap">
					<form id="wcap-abandoned-templates" method="get" >
						<input type="hidden" name="page" value="woocommerce_ac_page" />
						<input type="hidden" name="action" value="emailsettings" />
						<input type="hidden" name="wcap_action" value="emailsettings" />
						<input type="hidden" name="section" value="wcap_atc_settings" />
						<input type="hidden" name="wcap_section" value="wcap_atc_settings" />
						<?php $wcap_atc_template_list->display(); ?>
					</form>
				</div>
				<p>
					<strong><i>
						<?php esc_html_e( 'Email Captured: ', 'woocommercer-ac' ); ?>
					<i></strong>
					<?php esc_html_e( 'Number of email addresses captured using the Add to Cart template.', 'woocommerce-ac' ); ?>
				</p>
				<p>
					<strong><i>
						<?php esc_html_e( 'Viewed: ', 'woocommerce-ac' ); ?>
					</i></strong>
					<?php esc_html_e( 'Number of times the popup was displayed when Add to Cart button is clicked.', 'woocommerce-ac' ); ?>
				</p>
				<p>
					<strong><i>
						<?php esc_html_e( 'No Thanks: ', 'woocommerce-ac' ); ?>
					</i></strong>
					<?php esc_html_e( 'Number of times the user chose to not give their email address in the template.', 'woocommerce-ac' ); ?>
				</p>
			</div>
			<?php
		}
	}
}
