/**
 * Abandoned cart detail Modal
 */

var Modal;
var wcap_clicked_template_id;
var $wcap_template_status;
var $wcap_template_name;
var $wcap_template_views_cnt;
var $wcap_emails_captured_cnt;
var $wcap_redirected_cnt;
var $wcap_no_thanks_cnt; 
var $wcap_orders_processed;
var $wcap_coupons_applied_cnt;
var $wcap_phones_captured_cnt;

jQuery(function($) {

    Modal = {
        init: function(){

            $(document.body).on( 'click', '.wcap-js-close-modal', this.close );
            $(document.body).on( 'click', '.wcap-modal-overlay', this.close );
            $(document.body).on( 'click', '.wcap-js-open-modal', this.handle_link );
            $(document.body).on( 'mousedown', '.wcap-js-open-modal', this.handle_link_mouse_middle_click );
            
            $(window).resize(function(){
                Modal.position();
            });

            $(document).keydown(function(e) {
                if (e.keyCode == 27) {
                    Modal.close();
                }
            });

        },
        handle_link_mouse_middle_click: function( e ){
            
           if( e.which == 2 ) {
                var wcap_get_currentpage = window.location.href;    
                this.href = wcap_get_currentpage;
                e.preventDefault();
                return false;
           }
        },
        handle_link: function( e ){
            e.preventDefault();

            var $a = $( this );
            var current_page   = ''; 
            var wcap_get_currentpage = window.location.href;

            $wcap_template_status = $a.data('wcap-popup-status');
            template_body = '<div class="wcap-modal__body"> <div class="wcap-modal__body-inner"> </div> </div>';

            var type = $a.data('modal-type');
            
            if ( type == 'ajax' )
            {
                wcap_clicked_template_id     = $a.data('wcap-popup-id');
                Modal.open( 'type-ajax' );
                Modal.loading();
                var data = {
                    action                : 'wcap_popup_templates_info',
                    wcap_template_id      : wcap_clicked_template_id,
                }

                $.post( ajaxurl, data , function( response ){

                    Modal.contents( response ); 
                });
            }
        },
    
        open: function( classes ) {

            $(document.body).addClass('wcap-modal-open').append('<div class="wcap-modal-overlay"></div>');
            var modal_body = `
                <div class="wcap-modal ${classes}">
                    <div class="wcap-modal__contents">
                        <div class="wcap-modal__header">
                            <h1>Template #${wcap_clicked_template_id}</h1>
                            ${$wcap_template_status}
                        </div>
                        ${template_body}
                    </div>
                    <div class="wcap-icon-close wcap-js-close-modal"></div>
                </div>`;

            $(document.body).append( modal_body );

            this.position();
        },

        loading: function() {
            $(document.body).addClass('wcap-modal-loading');
        },

        contents: function ( contents ) {
            $(document.body).removeClass('wcap-modal-loading');

            contents = contents.replace(/\\(.)/mg, "$1");

            $('.wcap-modal__contents').html(contents);

            this.position();
        },

        close: function() {
            $(document.body).removeClass('wcap-modal-open wcap-modal-loading');
            
            $('.wcap-modal, .wcap-modal-overlay').remove();
        },

        position: function() {

            $('.wcap-modal__body').removeProp('style');

            var modal_header_height = $('.wcap-modal__header').outerHeight();
            var modal_height = $('.wcap-modal').height();
            var modal_width = $('.wcap-modal').width();
            var modal_body_height = $('.wcap-modal__body').outerHeight();
            var modal_contents_height = modal_body_height + modal_header_height;

            $('.wcap-modal').css({
                'margin-left': -modal_width / 2,
                'margin-top': -modal_height / 2
            });

            if ( modal_height < modal_contents_height - 5 ) {
                $('.wcap-modal__body').height( modal_height - modal_header_height );
            }
        },
    
    };
    Modal.init();
});