=== MyWorks Woo Sync for QuickBooks Desktop - Compatibility ===
Contributors: myworksdesign
Donate link: http://myworks.design
Tags: woocommerce, quickbooks, realtime, manual, sync, crm, pull, push, multilingual, multicurrency, multisite, product, tax, payment, customer, coupon, shipping
Requires at least: 4.0
Tested up to: 4.8
Stable tag: 4.8.
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

The only WooCommerce plugin to automatically sync your WooCommerce store to QuickBooks Desktop, all in real-time! Easily sync your orders, customers, inventory and more from your WooCommerce store to QuickBooks Desktop. Your complete solution to streamline your accounting workflow - with no limits.

== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Follow the step by step wizard to finish your installation and go live.

== Screenshots ==
 
1. This screen shot description corresponds to screenshot-1.(png|jpg). This describes plugin's dashboard

== Changelog ==

V 1.0.0
- First Launch
- Added compatibility with AvaTax