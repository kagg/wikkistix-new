<?php
if ( ! defined( 'ABSPATH' ) )
exit;

global $wpdb;
global $MWQDC_LB;

$page_url = 'admin.php?page=mw-qbo-desktop-sync-compt';

if ( ! empty( $_POST ) && check_admin_referer( 'desk_myworks_wc_qbo_sync_save_compt_stng', 'map_wc_qbo_update_compt_stng_desk' ) ) {
	//WooCommerce Sequential Order Numbers Pro
	if(isset($_POST['comp_wsnop'])){
		$mw_wc_qbo_desk_compt_p_wsnop = '';
		if(isset($_POST['mw_wc_qbo_desk_compt_p_wsnop'])){
			$mw_wc_qbo_desk_compt_p_wsnop = 'true';
		}
		update_option('mw_wc_qbo_desk_compt_p_wsnop',$mw_wc_qbo_desk_compt_p_wsnop);
	}
	
	//QuickBooks Desktop Advance Inventory Sync
	if(isset($_POST['comp_qais'])){
		$mw_wc_qbo_desk_compt_qbd_adv_invt_sync = '';
		if(isset($_POST['mw_wc_qbo_desk_compt_qbd_adv_invt_sync'])){
			$mw_wc_qbo_desk_compt_qbd_adv_invt_sync = 'true';
		}
		update_option('mw_wc_qbo_desk_compt_qbd_adv_invt_sync',$mw_wc_qbo_desk_compt_qbd_adv_invt_sync);
		
		
		$mw_wc_qbo_desk_compt_qbd_invt_site_ref = '';
		if(isset($_POST['mw_wc_qbo_desk_compt_qbd_invt_site_ref'])){			
			$mw_wc_qbo_desk_compt_qbd_invt_site_ref = trim($_POST['mw_wc_qbo_desk_compt_qbd_invt_site_ref']);
		}
		update_option('mw_wc_qbo_desk_compt_qbd_invt_site_ref',$mw_wc_qbo_desk_compt_qbd_invt_site_ref);
	}
	
	//WooCommerce Purchase Order Payment Gateway - PO Field
	if(isset($_POST['comp_wpopg_po'])){
		$mw_wc_qbo_desk_wpopg_po_support = '';
		if(isset($_POST['mw_wc_qbo_desk_wpopg_po_support'])){
			$mw_wc_qbo_desk_wpopg_po_support = 'true';
		}
		update_option('mw_wc_qbo_desk_wpopg_po_support',$mw_wc_qbo_desk_wpopg_po_support);
	}
	
	//WooCommerce Purchase Order Payment Gateway
	if(isset($_POST['comp_wacs_bc'])){
		$mw_wc_qbo_desk_wacs_base_cur_support = '';
		if(isset($_POST['mw_wc_qbo_desk_wacs_base_cur_support'])){
			$mw_wc_qbo_desk_wacs_base_cur_support = 'true';
		}
		update_option('mw_wc_qbo_desk_wacs_base_cur_support',$mw_wc_qbo_desk_wacs_base_cur_support);
	}
	
	//TaxJar - Sales Tax Automation for WooCommerce
	if(isset($_POST['comp_taxjar'])){
		$mw_wc_qbo_desk_wc_taxjar_support = '';
		if(isset($_POST['mw_wc_qbo_desk_wc_taxjar_support'])){
			$mw_wc_qbo_desk_wc_taxjar_support = 'true';
		}
		update_option('mw_wc_qbo_desk_wc_taxjar_support',$mw_wc_qbo_desk_wc_taxjar_support);
		
		$mw_wc_qbo_desk_wc_taxjar_map_qbo_product = '';
		if(isset($_POST['mw_wc_qbo_desk_wc_taxjar_map_qbo_product'])){			
			$mw_wc_qbo_desk_wc_taxjar_map_qbo_product = trim($_POST['mw_wc_qbo_desk_wc_taxjar_map_qbo_product']);
		}
		update_option('mw_wc_qbo_desk_wc_taxjar_map_qbo_product',$mw_wc_qbo_desk_wc_taxjar_map_qbo_product);
	}
	
	//WooCommerce AvaTax
	if(isset($_POST['comp_avatax'])){
		$mw_wc_qbo_desk_wc_avatax_support = '';
		if(isset($_POST['mw_wc_qbo_desk_wc_avatax_support'])){
			$mw_wc_qbo_desk_wc_avatax_support = 'true';
		}
		update_option('mw_wc_qbo_desk_wc_avatax_support',$mw_wc_qbo_desk_wc_avatax_support);
		
		$mw_wc_qbo_desk_wc_avatax_map_qbo_product = '';
		if(isset($_POST['mw_wc_qbo_desk_wc_avatax_map_qbo_product'])){
			$mw_wc_qbo_desk_wc_avatax_map_qbo_product = trim($_POST['mw_wc_qbo_desk_wc_avatax_map_qbo_product']);
		}
		update_option('mw_wc_qbo_desk_wc_avatax_map_qbo_product',$mw_wc_qbo_desk_wc_avatax_map_qbo_product);
	}
	
	$MWQDC_LB->set_session_val('compt_settings_save_msg',__('Compatibility settings saved successfully.','mw_wc_qbo_desk'));
	$MWQDC_LB->redirect($page_url);
}
$is_compt = false;
$list_selected = '';

$qbo_product_options = '';
if(!$MWQDC_LB->option_checked('mw_wc_qbo_desk_select2_ajax')){
	$qbo_product_options = $MWQDC_LB->option_html('', $wpdb->prefix.'mw_wc_qbo_desk_qbd_items','qbd_id','name','','name ASC','',true);
}

if(!$MWQDC_LB->option_checked('mw_wc_qbo_desk_select2_ajax')){
	$list_selected.='jQuery(\'#mw_wc_qbo_desk_wc_taxjar_map_qbo_product\').val(\''.$MWQDC_LB->get_option('mw_wc_qbo_desk_wc_taxjar_map_qbo_product').'\');';
	$list_selected.='jQuery(\'#mw_wc_qbo_desk_wc_avatax_map_qbo_product\').val(\''.$MWQDC_LB->get_option('mw_wc_qbo_desk_wc_avatax_map_qbo_product').'\');';
	
}

?>

<div class="container map-coupon-code-outer">
	<form method="post" action="<?php echo $page_url;?>">
	<?php wp_nonce_field( 'desk_myworks_wc_qbo_sync_save_compt_stng', 'map_wc_qbo_update_compt_stng_desk' ); ?>
		<!--WooCommerce Sequential Order Numbers Pro-->
		<!--WooCommerce Sequential Order Numbers-->
		<?php if($MWQDC_LB->is_plugin_active('woocommerce-sequential-order-numbers-pro','woocommerce-sequential-order-numbers')):?>
		<?php $is_compt=true;?>
		<div class="page_title">
			<?php
				$wsnop_p_name = 'WooCommerce Sequential Order Numbers Pro';
				if($MWQDC_LB->is_plugin_active('woocommerce-sequential-order-numbers')):
				$wsnop_p_name = 'WooCommerce Sequential Order Numbers';
			?>
			<h4 title="woocommerce-sequential-order-numbers"><?php _e( 'WooCommerce Sequential Order Numbers', 'mw_wc_qbo_desk' );?></h4>
			<?php else:?>
			<h4 title="woocommerce-sequential-order-numbers-pro"><?php _e( 'WooCommerce Sequential Order Numbers Pro', 'mw_wc_qbo_desk' );?></h4>
			<?php endif;?>
		</div>		
		<div class="card">
			<div class="card-content">
				<div class="col s12 m12 l12">
					<table class="mw-qbo-sync-settings-table menu-blue-bg" width="100%">
					<tr>
						<td colspan="3">
							<b><?php _e( 'Settings', 'mw_wc_qbo_desk' );?></b>
						</td>
					</tr>
					<tr>
						<td width="60%"><?php _e( 'Enable Sequential Order Number ', 'mw_wc_qbo_desk' );?> :</td>
						<td width="20%">
							<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_desk_compt_p_wsnop" id="mw_wc_qbo_desk_compt_p_wsnop" value="true" <?php if($MWQDC_LB->get_option('mw_wc_qbo_desk_compt_p_wsnop')=='true') echo 'checked' ?>>
						</td>
						<td width="20%">
							<div class="material-icons tooltipped tooltip">?
								<span class="tooltiptext">
									<?php _e( $wsnop_p_name, 'mw_wc_qbo_desk' );?>
								</span>
							</div>
						</td>						
					</tr>
					<tr>
						<td colspan="3">
							<input type="submit" name="comp_wsnop" class="waves-effect waves-light btn save-btn mw-qbo-sync-green" value="Save">
						</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<?php endif;?>
		
		<!--TaxJar - Sales Tax Automation for WooCommerce-->
		<?php if($MWQDC_LB->is_plugin_active('taxjar-simplified-taxes-for-woocommerce','taxjar-woocommerce')):?>
		<?php $is_compt=true;?>
		<div class="page_title">
			<h4 title="taxjar-simplified-taxes-for-woocommerce"><?php _e( 'TaxJar - Sales Tax Automation for WooCommerce', 'mw_wc_qbo_desk' );?></h4>
		</div>		
		<div class="card">
			<div class="card-content">
				<div class="col s12 m12 l12">
					<table class="mw-qbo-sync-settings-table menu-blue-bg" width="100%">
					<tr>
						<td colspan="3">
							<b><?php _e( 'Settings', 'mw_wc_qbo_desk' );?></b>
						</td>
					</tr>
					<tr>
						<td width="60%"><?php _e( 'Enable TaxJar Support', 'mw_wc_qbo_desk' );?> :</td>
						<td width="20%">
							<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_desk_wc_taxjar_support" id="mw_wc_qbo_desk_wc_taxjar_support" value="true" <?php if($MWQDC_LB->get_option('mw_wc_qbo_desk_wc_taxjar_support')=='true') echo 'checked' ?>>
						</td>
						<td width="20%">
							<div class="material-icons tooltipped tooltip">?
								<span class="tooltiptext">
									<?php _e( 'WooCommerce TaxJar', 'mw_wc_qbo_desk' );?>
								</span>
							</div>
						</td>						
					</tr>
					
					<tr>
						<td colspan="3">
							<b><?php _e( 'Mapping', 'mw_wc_qbo_desk' );?></b>
						</td>
					</tr>
					
					<tr>
						<td>TaxJar</td>
						
						<td>
							
							<?php
								$dd_options = '<option value=""></option>';
								$dd_ext_class = '';
								if($MWQDC_LB->option_checked('mw_wc_qbo_desk_select2_ajax')){
									$dd_ext_class = 'mwqs_dynamic_select_desk';
									if($MWQDC_LB->get_option('mw_wc_qbo_desk_wc_taxjar_map_qbo_product')!=''){
										$itemid = $MWQDC_LB->get_option('mw_wc_qbo_desk_wc_taxjar_map_qbo_product');
										$qb_item_name = $MWQDC_LB->get_field_by_val($wpdb->prefix.'mw_wc_qbo_desk_qbd_items','name','qbd_id',$itemid);
										if($qb_item_name!=''){
											$dd_options = '<option value="'.$itemid.'">'.$qb_item_name.'</option>';
										}
									}
								}else{
									$dd_options.=$qbo_product_options;
								}
							?>
							<select name="mw_wc_qbo_desk_wc_taxjar_map_qbo_product" id="mw_wc_qbo_desk_wc_taxjar_map_qbo_product" class="filled-in production-option mw_wc_qbo_desk_select <?php echo $dd_ext_class;?>">
								<?php echo $dd_options;?>
							</select>
						</td>
						
						<td>
							<div class="material-icons tooltipped tooltip">?
								<span class="tooltiptext">
									<?php _e( 'QBD Product', 'mw_wc_qbo_desk' );?>
								</span>
							</div>
						</td>
					</tr>
					
					<tr>
						<td colspan="3">
							<input type="submit" name="comp_taxjar" class="waves-effect waves-light btn save-btn mw-qbo-sync-green" value="Save">
						</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<?php endif;?>
		
		<!--WooCommerce AvaTax-->
		<?php if($MWQDC_LB->is_plugin_active('woocommerce-avatax')):?>
		<?php $is_compt=true;?>
		<div class="page_title">
			<h4 title="woocommerce-avatax"><?php _e( 'WooCommerce AvaTax', 'mw_wc_qbo_desk' );?></h4>
		</div>		
		<div class="card">
			<div class="card-content">
				<div class="col s12 m12 l12">
					<table class="mw-qbo-sync-settings-table menu-blue-bg" width="100%">
					<tr>
						<td colspan="3">
							<b><?php _e( 'Settings', 'mw_wc_qbo_desk' );?></b>
						</td>
					</tr>
					<tr>
						<td width="60%"><?php _e( 'Enable AvaTax Support', 'mw_wc_qbo_desk' );?> :</td>
						<td width="20%">
							<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_desk_wc_avatax_support" id="mw_wc_qbo_desk_wc_avatax_support" value="true" <?php if($MWQDC_LB->get_option('mw_wc_qbo_desk_wc_avatax_support')=='true') echo 'checked' ?>>
						</td>
						<td width="20%">
							<div class="material-icons tooltipped tooltip">?
								<span class="tooltiptext">
									<?php _e( 'WooCommerce AvaTax', 'mw_wc_qbo_desk' );?>
								</span>
							</div>
						</td>						
					</tr>
					
					<tr>
						<td colspan="3">
							<b><?php _e( 'Mapping', 'mw_wc_qbo_desk' );?></b>
						</td>
					</tr>
					
					<tr>
						<td>AvaTax</td>
						
						<td>
							
							<?php
								$dd_options = '<option value=""></option>';
								$dd_ext_class = '';
								if($MWQDC_LB->option_checked('mw_wc_qbo_desk_select2_ajax')){
									$dd_ext_class = 'mwqs_dynamic_select_desk';
									if($MWQDC_LB->get_option('mw_wc_qbo_desk_wc_avatax_map_qbo_product')!=''){
										$itemid = $MWQDC_LB->get_option('mw_wc_qbo_desk_wc_avatax_map_qbo_product');
										$qb_item_name = $MWQDC_LB->get_field_by_val($wpdb->prefix.'mw_wc_qbo_desk_qbd_items','name','qbd_id',$itemid);
										if($qb_item_name!=''){
											$dd_options = '<option value="'.$itemid.'">'.$qb_item_name.'</option>';
										}
									}
								}else{
									$dd_options.=$qbo_product_options;
								}
							?>
							<select name="mw_wc_qbo_desk_wc_avatax_map_qbo_product" id="mw_wc_qbo_desk_wc_avatax_map_qbo_product" class="filled-in production-option mw_wc_qbo_desk_select <?php echo $dd_ext_class;?>">
								<?php echo $dd_options;?>
							</select>
						</td>
						
						<td>
							<div class="material-icons tooltipped tooltip">?
								<span class="tooltiptext">
									<?php _e( 'QBD Product', 'mw_wc_qbo_desk' );?>
								</span>
							</div>
						</td>
					</tr>
					
					<tr>
						<td colspan="3">
							<input type="submit" name="comp_avatax" class="waves-effect waves-light btn save-btn mw-qbo-sync-green" value="Save">
						</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<?php endif;?>
		
		<!--Aelia Currency Switcher for WooCommerce-->
		<?php if($MWQDC_LB->is_plugin_active('woocommerce-aelia-currencyswitcher')):?>
		<?php $is_compt=true;?>
		<div class="page_title">
			<h4 title="woocommerce-aelia-currencyswitcher"><?php _e( 'Aelia Currency Switcher for WooCommerce', 'mw_wc_qbo_desk' );?></h4>
		</div>		
		<div class="card">
			<div class="card-content">
				<div class="col s12 m12 l12">
					<table class="mw-qbo-sync-settings-table menu-blue-bg" width="100%">
					<tr>
						<td colspan="3">
							<b><?php _e( 'Settings', 'mw_wc_qbo_desk' );?></b>
						</td>
					</tr>
					<tr>
						<td width="60%"><?php _e( 'Enable Base Currency Support', 'mw_wc_qbo_desk' );?> :</td>
						<td width="20%">
							<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_desk_wacs_base_cur_support" id="mw_wc_qbo_desk_wacs_base_cur_support" value="true" <?php if($MWQDC_LB->get_option('mw_wc_qbo_desk_wacs_base_cur_support')=='true') echo 'checked' ?>>
						</td>
						<td width="20%">
							<div class="material-icons tooltipped tooltip">?
								<span class="tooltiptext">
									<?php _e( 'Base Currency Support', 'mw_wc_qbo_desk' );?>
								</span>
							</div>
						</td>						
					</tr>					
					
					<tr>
						<td colspan="3">
							<input type="submit" name="comp_wacs_bc" class="waves-effect waves-light btn save-btn mw-qbo-sync-green" value="Save">
						</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<?php endif;?>
		
		
		<!--WooCommerce Purchase Order Payment Gateway-->
		<?php if($MWQDC_LB->is_plugin_active('woocommerce-gateway-purchase-order')):?>
		<?php $is_compt=true;?>
		<div class="page_title">
			<h4 title="woocommerce-gateway-purchase-order"><?php _e( 'WooCommerce Purchase Order Payment Gateway', 'mw_wc_qbo_desk' );?></h4>
		</div>		
		<div class="card">
			<div class="card-content">
				<div class="col s12 m12 l12">
					<table class="mw-qbo-sync-settings-table menu-blue-bg" width="100%">
					<tr>
						<td colspan="3">
							<b><?php _e( 'Settings', 'mw_wc_qbo_desk' );?></b>
						</td>
					</tr>
					<tr>
						<td width="60%"><?php _e( 'Enable PO Number Support', 'mw_wc_qbo_desk' );?> :</td>
						<td width="20%">
							<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_desk_wpopg_po_support" id="mw_wc_qbo_desk_wpopg_po_support" value="true" <?php if($MWQDC_LB->get_option('mw_wc_qbo_desk_wpopg_po_support')=='true') echo 'checked' ?>>
						</td>
						<td width="20%">
							<div class="material-icons tooltipped tooltip">?
								<span class="tooltiptext">
									<?php _e( 'PO Number Support', 'mw_wc_qbo_desk' );?>
								</span>
							</div>
						</td>						
					</tr>					
					
					<tr>
						<td colspan="3">
							<input type="submit" name="comp_wpopg_po" class="waves-effect waves-light btn save-btn mw-qbo-sync-green" value="Save">
						</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<?php endif;?>
		
		<?php $qbd_ais_active = true;?>
		
		<?php if($qbd_ais_active):?>
		<?php $is_compt=true;?>
		<div class="page_title">			
			<h4 title="QuickBooks Desktop Advanced Inventory"><?php _e( 'QuickBooks Desktop Advanced Inventory', 'mw_wc_qbo_desk' );?></h4>
		</div>		
		<div class="card">
			<div class="card-content">
				<div class="col s12 m12 l12">
					<table class="mw-qbo-sync-settings-table menu-blue-bg" width="100%">
					<tr>
						<td colspan="3">
							<b><?php _e( 'Settings', 'mw_wc_qbo_desk' );?></b>
						</td>
					</tr>
					<tr>
						<td width="60%"><?php _e( 'Enable Advanced Inventory Features ', 'mw_wc_qbo_desk' );?> :</td>
						<td width="20%">
							<input type="checkbox" class="filled-in mwqs_st_chk  production-option" name="mw_wc_qbo_desk_compt_qbd_adv_invt_sync" id="mw_wc_qbo_desk_compt_qbd_adv_invt_sync" value="true" <?php if($MWQDC_LB->get_option('mw_wc_qbo_desk_compt_qbd_adv_invt_sync')=='true') echo 'checked' ?>>
						</td>
						<td width="20%">
							<div class="material-icons tooltipped tooltip">?
								<span class="tooltiptext">
									<?php _e( 'Turn this switch on to enable compatibility with QuickBooks Desktop Advanced Inventory.', 'mw_wc_qbo_desk' );?>
								</span>
							</div>
						</td>						
					</tr>					
					
					<tr>
						<th class="title-description">
							<?php echo __('Inventory Site','mw_wc_qbo_desk') ?>
							
						</th>
						<td>
							<div class="row">
								<div class="input-field col s12 m12 l12">
									<select name="mw_wc_qbo_desk_compt_qbd_invt_site_ref" id="mw_wc_qbo_desk_compt_qbd_invt_site_ref" class="mw_wc_qbo_desk_select">
									<option value=""></option>
									<?php echo $MWQDC_LB->option_html($MWQDC_LB->get_option('mw_wc_qbo_desk_compt_qbd_invt_site_ref'), $wpdb->prefix.'mw_wc_qbo_desk_qbd_list_inventorysite','qbd_id','name','','name ASC','',true);?>
									</select>
								</div>
							</div>
						</td>
						<td>
							<div class="material-icons tooltipped right tooltip"><?php echo __('?','mw_wc_qbo_desk') ?>
							  <span class="tooltiptext"><?php echo __('Choose a QuickBooks Desktop Inventory Site to assign all orders synced through our integration to.','mw_wc_qbo_desk') ?></span>
							</div>
						</td>
					</tr>
					
					
					<tr>
						<td colspan="3">
							<input type="submit" name="comp_qais" class="waves-effect waves-light btn save-btn mw-qbo-sync-green" value="Save">
						</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<?php endif;?>
		
	</form>
	
	<!--If No Plugin-->
	<?php if(!$is_compt):?>
	<table width="100%">
		<tr>
			<td colspan="3">
				<b><?php _e( 'No Compatibility Plugins Active', 'mw_wc_qbo_sync' );?></b>
			</td>
		</tr>
	</table>
	<?php endif;?>
	
</div>

<?php echo $MWQDC_LB->get_admin_get_extra_css_js();?>
<?php echo $MWQDC_LB->get_checkbox_switch_css_js();?>

<script type="text/javascript">
jQuery(document).ready(function($){
	<?php echo $list_selected;?>
	jQuery('input.mwqs_st_chk').attr('data-size','small');
	jQuery('input.mwqs_st_chk').bootstrapSwitch();
});
</script>

<?php echo $MWQDC_LB->get_select2_js('.mw_wc_qbo_desk_select','qbo_product');?>

<?php
if($save_status = $MWQDC_LB->get_session_val('compt_settings_save_msg','',true)){
	$save_status = ($save_status!='')?$save_status:'error';
	$MWQDC_LB->set_admin_sweet_alert($save_status);
}
?>
