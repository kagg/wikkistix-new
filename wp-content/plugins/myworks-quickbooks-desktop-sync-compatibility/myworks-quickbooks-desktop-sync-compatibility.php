<?php 
/**
 * Plugin Name:       MyWorks Woo Sync for QuickBooks Desktop - Compatibility
 * Plugin URI:        https://myworks.software/integrations/sync-woocommerce-quickbooks-desktop/
 * Description:       Extend compatibility for your MyWorks WooCommerce Sync for QuickBooks Desktop Plugin
 * Version:           1.0.0
 * Author:            MyWorks Design
 * Author URI:        http://myworks.design/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mw_wc_qbo_sync_compt_desktop
 * Domain Path:       /languages
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mwqbosync_compt_desktop_plugin_avtivation(){

  $active_plugins = get_option('active_plugins');
  if(in_array('myworks-quickbooks-desktop-sync/myworks-quickbooks-desktop-sync.php',$active_plugins)){
  	return true;
  }else{
  	$error_message = __('This plugin requires <a target="_blank" href="https://myworks.software/integrations/sync-woocommerce-quickbooks-desktop/">MyWorks Woo Sync for QuickBooks Desktop</a> plugin to be active!', 'mw_wc_qbo_desk');
	die($error_message);
  }
}

register_activation_hook(__FILE__, 'mwqbosync_compt_desktop_plugin_avtivation');

function mwqbosync_compt_desktop_plugin_deactivation(){	
	return true;
}

register_deactivation_hook( __FILE__, 'mwqbosync_compt_desktop_plugin_deactivation' );

add_action('admin_menu', 'mwqbosync_compt_desktop_plugin_add_sub_menu', 11 );

function mwqbosync_compt_desktop_plugin_add_sub_menu(){	
	if(class_exists('MW_QBO_Desktop_Sync_Lib')){		
		add_submenu_page(
			'mw-qbo-desktop', 
			__( 'Compatibility', 'mw_wc_qbo_desk' ),
			__( 'Compatibility', 'mw_wc_qbo_desk' ),
			'manage_options',
			'mw-qbo-desktop-sync-compt',
			'mwqbosync_compt_desktop_plugin_add_compt_menu'
		);
	}		
}	

function mwqbosync_compt_desktop_plugin_add_compt_menu(){
	require_once plugin_dir_path( __FILE__ ) . 'myworks-quickbooks-desktop-sync-admin-compt.php';
}

require_once('wp-updates-plugin.php');
new WPUpdatesPluginUpdater_1784( 'http://wp-updates.com/api/2/plugin', plugin_basename(__FILE__));
