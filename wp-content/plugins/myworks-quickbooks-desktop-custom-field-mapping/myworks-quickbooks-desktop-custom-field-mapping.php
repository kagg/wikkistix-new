<?php 
/**
 * Plugin Name:       MyWorks Woo Sync for QuickBooks Desktop - Custom Field Mapping
 * Plugin URI:        https://myworks.software/integrations/sync-woocommerce-quickbooks-desktop/
 * Description:       Add custom field mapping functionality for your MyWorks WooCommerce Sync for QuickBooks Desktop Plugin
 * Version:           1.0.0
 * Author:            MyWorks Design
 * Author URI:        http://myworks.design/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mw_wc_qbo_sync_cfm
 * Domain Path:       /languages
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function mwqbdesktopsync_cfm_plugin_avtivation(){

  $active_plugins = get_option('active_plugins');
  if(in_array('myworks-quickbooks-desktop-sync/myworks-quickbooks-desktop-sync.php',$active_plugins)){
	update_option('mw_wc_qbo_desk_sh_cfm_hash','d33e21c644cd824e2d86df42dd90a39d3f926a8a');
  	return true;
  }else{
  	$error_message = __('This plugin requires <a target="_blank" href="https://myworks.software/integrations/sync-woocommerce-quickbooks-desktop/">MyWorks Woo Sync for QuickBooks Desktop</a> plugin to be active!', 'mw_wc_qbo_desk');
	die($error_message);
  }  
}
 
register_activation_hook(__FILE__, 'mwqbdesktopsync_cfm_plugin_avtivation');

function mwqbdesktopsync_cfm_plugin_deactivation(){
	delete_option('mw_wc_qbo_desk_sh_cfm_hash');
	return true;
}

register_deactivation_hook( __FILE__, 'mwqbdesktopsync_cfm_plugin_deactivation' );