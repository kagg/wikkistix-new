<?php

/**

 * Logo template.

 *

 * @author     ThemeFusion

 * @copyright  (c) Copyright by ThemeFusion

 * @link       http://theme-fusion.com

 * @package    Avada

 * @subpackage Core

 */



// Do not allow directly accessing this file.

if ( ! defined( 'ABSPATH' ) ) {

	exit( 'Direct script access denied.' );

}

$logo_opening_markup = '<div class="';

$logo_closing_markup = '</div>';

if ( 'v7' === Avada()->settings->get( 'header_layout' ) && ! Avada()->settings->get( 'logo_background' ) ) {

	$logo_opening_markup = '<li class="fusion-middle-logo-menu-logo ';

	$logo_closing_markup = '</li>';

} elseif ( 'v7' === Avada()->settings->get( 'header_layout' ) && Avada()->settings->get( 'logo_background' ) && 'Top' === Avada()->settings->get( 'header_position' ) ) {

	$logo_opening_markup = '<li class="fusion-logo-background fusion-middle-logo-menu-logo"><div class="';

	$logo_closing_markup = '</div></li>';

} elseif ( Avada()->settings->get( 'logo_background' ) && 'v4' !== Avada()->settings->get( 'header_layout' ) && 'v5' !== Avada()->settings->get( 'header_layout' ) && 'Top' === Avada()->settings->get( 'header_position' ) ) {

	$logo_opening_markup = '<div class="fusion-logo-background"><div class="';

	$logo_closing_markup = '</div></div>';

}



// @codingStandardsIgnoreLine

echo $logo_opening_markup; ?>fusion-logo" data-margin-top="<?php echo esc_attr( Avada()->settings->get( 'logo_margin', 'top' ) ); ?>" data-margin-bottom="<?php echo esc_attr( Avada()->settings->get( 'logo_margin', 'bottom' ) ); ?>" data-margin-left="<?php echo esc_attr( Avada()->settings->get( 'logo_margin', 'left' ) ); ?>" data-margin-right="<?php echo esc_attr( Avada()->settings->get( 'logo_margin', 'right' ) ); ?>">

	<?php

	/**

	 * The avada_logo_prepend hook.

	 */

	do_action( 'avada_logo_prepend' );

	?>

	<?php if ( ( Avada()->settings->get( 'logo', 'url' ) && '' !== Avada()->settings->get( 'logo', 'url' ) ) || ( Avada()->settings->get( 'logo_retina', 'url' ) && '' !== Avada()->settings->get( 'logo_retina', 'url' ) ) ) : ?>

		<a class="fusion-logo-link" href="<?php echo esc_url_raw( home_url( '/' ) ); ?>">

			<?php

			$logo_url = Fusion_Sanitize::get_url_with_correct_scheme( Avada()->settings->get( 'logo', 'url' ) );



			// Use retina logo, if default one is not set.

			if ( '' === $logo_url ) {

				$logo_url = Fusion_Sanitize::get_url_with_correct_scheme( Avada()->settings->get( 'logo_retina', 'url' ) );

			$logo_data = Avada()->get_images_obj()->get_logo_data( 'logo_retina' );

				$logo_data['style'] = '';

				if ( '' !== $logo_data['width'] ) {

					$logo_data['style'] = ' style="max-height:' . $logo_data['height'] . 'px;height:auto;"';

				}

			} else {

				$logo_data = Avada()->get_images_obj()->get_logo_data( 'logo' );

				$logo_data['style'] = '';

			}



			$logo_size['width']  = $logo_data['width'];

			$logo_size['height'] = $logo_data['height'];

			?>

			<!--<img src="<?php echo esc_url_raw( $logo_url ); ?>" width="<?php echo esc_attr( $logo_size['width'] ); ?>" height="<?php echo esc_attr( $logo_size['height'] ); ?>"<?php echo wp_kses_post( $logo_data['style'] ); ?> alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Logo', 'Avada' ); ?>" class="fusion-logo-1x fusion-standard-logo" />-->



			<?php /*if ( Avada()->settings->get( 'logo_retina', 'url' ) && '' !== Avada()->settings->get( 'logo_retina', 'url' ) && '' !== Avada()->settings->get( 'logo_retina', 'width' ) && '' !== Avada()->settings->get( 'logo_retina', 'height' ) ) : ?>

				<?php $retina_logo = Fusion_Sanitize::get_url_with_correct_scheme( Avada()->settings->get( 'logo_retina', 'url' ) ); ?>

				<?php $style = 'style="max-height: ' . $logo_size['height'] . 'px; height: auto;"'; ?>

				<!--<img src="<?php echo esc_url_raw( $retina_logo ); ?>" width="<?php echo esc_attr( $logo_size['width'] ); ?>" height="<?php echo esc_attr( $logo_size['height'] ); ?>" alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Retina Logo', 'Avada' ); ?>" <?php echo wp_kses_post( $style ); ?> class="fusion-standard-logo fusion-logo-2x" />-->

			<?php else : ?>

				<!--<img src="<?php echo esc_url_raw( $logo_url ); ?>" width="<?php echo esc_attr( $logo_size['width'] ); ?>" height="<?php echo esc_attr( $logo_size['height'] ); ?>" alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Retina Logo', 'Avada' ); ?>" class="fusion-standard-logo fusion-logo-2x" />-->

			<?php endif; */?>



			<!-- mobile logo -->

			<?php if ( Avada()->settings->get( 'mobile_logo', 'url' ) && '' !== Avada()->settings->get( 'mobile_logo', 'url' ) ) : ?>

				<?php $mobile_logo_data = Avada()->get_images_obj()->get_logo_data( 'mobile_logo' ); ?>

				<img src="<?php echo esc_url_raw( $mobile_logo_data['url'] ); ?>" width="<?php echo esc_attr( $mobile_logo_data['width'] ); ?>" height="<?php echo esc_attr( $mobile_logo_data['height'] ); ?>" alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Mobile Logo', 'Avada' ); ?>" class="fusion-logo-1x fusion-mobile-logo-1x" />



				<?php if ( Avada()->settings->get( 'mobile_logo_retina', 'url' ) && '' != Avada()->settings->get( 'mobile_logo_retina', 'url' ) && '' != Avada()->settings->get( 'mobile_logo_retina', 'width' ) && '' != Avada()->settings->get( 'mobile_logo_retina', 'height' ) ) : ?>

					<?php

					$retina_mobile_logo_data = Avada()->get_images_obj()->get_logo_data( 'mobile_logo_retina' );

					$style = 'style="max-height: ' . $retina_mobile_logo_data['height'] . 'px; height: auto;"';

					?>

					<img src="<?php echo esc_url_raw( $retina_mobile_logo_data['url'] ); ?>" width="<?php echo esc_attr( $retina_mobile_logo_data['width'] ); ?>" height="<?php echo esc_attr( $retina_mobile_logo_data['height'] ); ?>" alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Mobile Retina Logo', 'Avada' ); ?>" <?php echo wp_kses_post( $style ); ?> class="fusion-logo-2x fusion-mobile-logo-2x" />

				<?php else : ?>

					<img src="<?php echo esc_url_raw( $mobile_logo_data['url'] ); ?>" width="<?php echo esc_attr( $mobile_logo_data['width'] ); ?>" height="<?php echo esc_attr( $mobile_logo_data['height'] ); ?>" alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Mobile Retina Logo', 'Avada' ); ?>" class="fusion-logo-2x fusion-mobile-logo-2x" />

				<?php endif; ?>

			<?php endif; ?>



			<!-- sticky header logo -->

			<?php if ( Avada()->settings->get( 'sticky_header_logo', 'url' ) && '' !== Avada()->settings->get( 'sticky_header_logo', 'url' ) && ( in_array( Avada()->settings->get( 'header_layout' ), array( 'v1', 'v2', 'v3', 'v6', 'v7' ) ) || ( ( in_array( Avada()->settings->get( 'header_layout' ), array( 'v4', 'v5' ) ) && 'menu_and_logo' === Avada()->settings->get( 'header_sticky_type2_layout' ) ) ) ) ) : ?>

				<?php $sticky_logo_data = Avada()->get_images_obj()->get_logo_data( 'sticky_header_logo' ); ?>

				<img src="<?php echo esc_url_raw( $sticky_logo_data['url'] ); ?>" width="<?php echo esc_attr( $sticky_logo_data['width'] ); ?>" height="<?php echo esc_attr( $sticky_logo_data['height'] ); ?>" alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Sticky Logo', 'Avada' ); ?>" class="fusion-logo-1x fusion-sticky-logo-1x" />



				<?php if ( Avada()->settings->get( 'sticky_header_logo_retina', 'url' ) && '' !== Avada()->settings->get( 'sticky_header_logo_retina', 'url' ) && '' !== Avada()->settings->get( 'sticky_header_logo_retina', 'width' ) && '' !== Avada()->settings->get( 'sticky_header_logo_retina', 'height' ) ) : ?>

					<?php

					$retina_sticky_logo_data = Avada()->get_images_obj()->get_logo_data( 'sticky_header_logo_retina' );

					$style = 'style="max-height: ' . $retina_sticky_logo_data['height'] . 'px; height: auto;"';

					?>

					<img src="<?php echo esc_url_raw( $retina_sticky_logo_data['url'] ); ?>" width="<?php echo esc_attr( $retina_sticky_logo_data['width'] ); ?>" height="<?php echo esc_attr( $retina_sticky_logo_data['height'] ); ?>" alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Sticky Logo Retina', 'Avada' ); ?>" <?php echo wp_kses_post( $style ); ?> class="fusion-logo-2x fusion-sticky-logo-2x" />

				<?php else : ?>

					<img src="<?php echo esc_url_raw( $sticky_logo_data['url'] ); ?>" width="<?php echo esc_attr( $sticky_logo_data['width'] ); ?>" height="<?php echo esc_attr( $sticky_logo_data['height'] ); ?>" alt="<?php bloginfo( 'name' ); ?> <?php esc_attr_e( 'Sticky Logo Retina', 'Avada' ); ?>" class="fusion-logo-2x fusion-sticky-logo-2x" />

				<?php endif; ?>

			<?php endif; ?>

		</a>

	<?php endif; ?>

	<?php // 23-03-2017 EMM edit for New heade Logo & banner 04-11-2016 #starts ?>



<style type="text/css">



.column-left-wikki{ float: left; width: 33%; text-align: left; padding-top: 5px; }



.column-right-wikki{ float: right; width: 33%; text-align: right; padding-top: 5px; padding-bottom: 14px;}



.column-center-wikki{ float:left; width: 33%; text-align: left; padding-top: 5px; }



#header-banner{width:100%;}

.home {margin-top: 0px !important;}

@media only screen and (max-width: 1023px) {
.column-right-wikki{padding-bottom: 35px;}
}

@media only screen and (max-width: 667px) {
.column-right-wikki{text-align: center;}
}

</style>           



<div id="header-banner">


<div class="column-center-wikki"><a href="https://www.wikkistix.com/"><img src="/wp-content/uploads/2022/07/wikki-stix-logo.png" width="260" height="238" alt="Wikki Stix One of A Kind Creatables"/></a></div>


<div class="column-left-wikki"><a href="https://www.wikkistix.com/what-are-wikki-stix/"><img src="/wp-content/uploads/2022/07/how-do-they-work-r.png" width="357" height="238" alt="What are Wikki Stix?"/></a></div>




<div class="column-right-wikki">

<img src="/wp-content/uploads/2022/07/made-in-the-usa-r.png" width="316" height="238" alt="Endlessly reusuable, self-stick, no-glue and Made in the USA"/></div> 



</div>



<?php // 28-02-2017 EMM edit for New heade Logo & banner #ends ?>     



  <?php // //28-02-2017 EMM to display search menu on mobile on header #starts ?>		



<style> 



    #mobileShoppingCartIcon ,#mobilesearch{



         display: none;



    }



	@media screen and (max-width: 800px) {







		#mobilesearch {



		display: block;



		margin: auto;



		position: relative;



		width: 62%;


		z-index: 99999;



	}



        .column-left-wikki {



            padding-bottom: 0px !important;



        }



		#mobileShoppingCartIcon {



			clear: both;



			display: block;



			position: relative;



			text-align: center;



		}



		#mobileShoppingCartIcon > a:before {



			content: "?";



			font-family: "IcoMoon";



			padding: 5px;



			color:#333;



		}



		   



		#mobileShoppingCartIcon > a.fullcartMobile:before{



			color:#e82c67;



		}



    }



</style>



<script>



	var cartIcon = function(){ 



		if(jQuery('nav#nav li.cart div.cart-contents').find('div.cart-content').length > 0 ){



			jQuery('#mobileShoppingCartIcon').find('a').addClass('fullcartMobile'); 



		}



		else{



			jQuery('#mobileShoppingCartIcon').find('a').removeClass('fullcartMobile'); 



		}



	}



	jQuery(document).ready(function(){



		cartIcon();



		jQuery('a.add_to_cart_button').click(function(){



			var delay=3000;



			setTimeout(cartIcon,delay); 



		});



	});					



</script>



    <div id="mobileShoppingCartIcon">



        <?php global $woocommerce; ?>



        <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>">Shopping Cart</a> 



    </div>



	<div id="mobilesearch">



<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">

	<label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>

	<input type="search" class="search-field" style="width:75%;float: left;" placeholder="<?php echo esc_attr_x( 'Search Products&hellip;', 'placeholder', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" />

	<button type="submit" style="width:25%;float: left; border: 1px solid #000; padding: 10px; margin-top: 10px; background:#000; color:#fff;" value="<?php echo esc_attr_x( 'Go', 'submit button', 'woocommerce' ); ?>" /><i class="fa fa-search" aria-hidden="true"></i></button>

	<input type="hidden" name="post_type" value="product" />

</form>



	</div>



<?php //23-03-2017 EMM to display search menu on mobile on header #ends ?>  

    

<?php

	/**

	 * The avada_logo_append hook.

	 *

	 * @hooked avada_header_content_3 - 10.

	 */

	do_action( 'avada_logo_append' );



	?>

<?php echo wp_kses_post( $logo_closing_markup );



/* Omit closing PHP tag to avoid "Headers already sent" issues. */

