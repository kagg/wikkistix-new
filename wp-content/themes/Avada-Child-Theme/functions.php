<?php

function avada_child_scripts() {

	if ( ! is_admin() && ! in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {

		$theme_info = wp_get_theme();

		wp_enqueue_style( 'avada-child-stylesheet', get_template_directory_uri() . '/style.css', array(), $theme_info->get( 'Version' ) );		

	}

	wp_enqueue_script( 'avada-child-stylesheet', get_stylesheet_directory_uri() . '/checkout.js', array(), '1.0.0', true );

}

add_action('wp_enqueue_scripts', 'avada_child_scripts');



add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );



function woo_remove_product_tabs( $tabs ) {



unset( $tabs['additional_information'] ); // Remove the additional information tab



return $tabs;



} 



add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );



// Our hooked in function - $fields is passed via the filter!





function custom_override_checkout_fields( $fields ) {

    /*$fields['billing']['billing_is_shipping_address'] = array(

    	'type'	=>	'checkbox',	

    	'value' =>  1,

        'label'     => __('This is my shipping address.', 'woocommerce'),

        'required'  => false,

        'class'     => array('form-row-wide hidemee'),

        'clear'     => true,

    	'checked'	=> 'checked'

     );*/ //commented on 29-11-2016
     
    /*  $fields['billing']['address_type'] = array(
            'label'     => __('Shipping Address Type (you must select one)*','woocommerce'),
            'class'     => array ('update_totals_on_change','addr_type_class'),
            'type'      => 'radio',
            'options'   => array("Residential Address","Business Address"),
     );*/   //commented on 06.04.2018 by EMM
     
    
     
    $fields['billing']['billing_company']['class'] =  array ('address-field', 'update_totals_on_change' );

    $fields['shipping']['shipping_company']['class'] =  array ('address-field', 'update_totals_on_change' );

    return $fields;

} 

/**
 * Process the checkout
 */
/*add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
    // Check if set, if its not set add an error.
    if ( !isset($_POST['address_type']) ||  $_POST['address_type'] == '' )
        wc_add_notice( __( 'Shipping Address Type is a required field.' ), 'error' );
}*/  //commented on 06.04.2018 by EMM

// free shipping modification 24 Nov 2014 BOF 



add_filter( 'woocommerce_package_rates', 'free_shipping_except' , 10, 2 );



function free_shipping_except( $rates, $package ) {

	///////////////////////////////////// flat rate bof

	global $woocommerce;
    ////virtual product exlude bof/	

		$giftCardTotal=0;

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ){ 

			if($cart_item['product_id']==11220){

				$_product	 = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

				$giftCardTotal = $giftCardTotal + $_product->get_price_excluding_tax() * $cart_item['quantity'];

			}			

		}

	////virtual product exlude eof//

	$cart_subtotal = WC()->cart->cart_contents_total - $giftCardTotal;
    // Added  by emm 11.04.2018
	if($cart_subtotal > 10){

		unset( $rates['flat_rate:1'] );

	} 
        $valid_for_cart = true;
        $exluded_product_cat = array('260','259');
        $product_categories = array();
       	if ( ! WC()->cart->is_empty() ) {
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ){ 
		  $product_cats = wc_get_product_cat_ids( $cart_item['product_id'] );       
          //$product_categories = $product_cats;   
          if ( sizeof( array_intersect( $product_cats, $exluded_product_cat ) ) > 0 ) {
						$valid_for_cart = false;                       
				}              
          }  
         }  
         
         
         
         // print_r($product_categories);
          //if (in_array( "260", $product_categories)) {
		//				$valid_for_cart = false;                                      
		//		} 
                   
               
        //foreach($exluded_product_cat as $key => $value ){  
         //      if (in_array( $value, $product_categories)) {
		//				$valid_for_cart = false;
                                                                                    
		//		}          
          //}
                
              // var_dump($valid_for_cart);
                     
        if ( ! $valid_for_cart ) {
		  unset( $rates['flat_rate:1'] );
		 	} 
    
    $user_country = WC()->customer->shipping_country;
     if ($user_country != "US"){
         unset( $rates['flat_rate:1'] );
     }
    //emm end 03.03.2017

    

	/////////////////////////////////////flat rate eof

	$excluded_states = array( 'AK','HI' );

	if( (isset( $rates['legacy_free_shipping'] ) || isset($rates['flat_rate:1'])) AND in_array( WC()->customer->shipping_state, $excluded_states ) ) {

    unset( $rates['legacy_free_shipping'] );

		unset( $rates['flat_rate:1'] );

	} 
    

	//print_r($rates);exit;

	//die(WC()->customer->shipping_country);  

	$fedex_ground_country = array('CA');

	if(in_array( WC()->customer->shipping_country, $fedex_ground_country ) ) {

		unset( $rates['fedex:FEDEX_GROUND'] ); 	

	}

	return $rates;

} 

// free shipping modification 24 Nov 2014 EOF  

// coupon message international shipping 24 Nov 2014 BOF  

add_filter('woocommerce_coupon_is_valid', 'invalid_country_coupon', 10, 2 );

function invalid_country_coupon( $valid, $coupon) {

	/*if ( $valid && $coupon->exclude_sale_items == 'yes' ) {

		$product_ids_on_sale = wc_get_product_ids_on_sale();

		if ( sizeof( WC()->cart->get_cart() ) > 0 ) {

			foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

	if ( in_array( $cart_item['product_id'], $product_ids_on_sale, false ) || in_array( $cart_item['variation_id'], $product_ids_on_sale, false ) || in_array( $cart_item['data']->get_parent(), $product_ids_on_sale, false ) ) {

				return false;

				}

			}

	}

	}*/

	global $woocommerce;



	//$shipCountry = $woocommerce->customer->get_country();



	$shipCountry = WC()->customer->shipping_country; 

	$shipState = WC()->customer->shipping_state; 



	$excluded_states = array( 'AK','HI' );

        $coupons = WC()->cart->get_coupons();



	if(($shipCountry != 'US' || in_array($shipState, $excluded_states)) && $coupon->enable_free_shipping() ){



	//if($shipCountry != 'US' || in_array($shipState, $excluded_states))  {

		global $woocommerce;



		wc_clear_notices();



        wc_add_notice( sprintf( __( 'Free Shipping Coupon is not available for your address.', "your-theme-language" ) ) ,'error' );

		//$woocommerce->show_messages();



		$woocommerce->cart->remove_coupons(); 



 		//echo '<script> jQuery(".woocommerce-error li").eq(1).remove();</script>';



		return false;            

	}

	return $valid;

	//return false;

	//$customer = new WC_Customer( $order_id );



	//return $valid;

}



// code added for short shipping method by cost Emm 19.10.2010 Beg...

add_filter( 'woocommerce_package_rates' , 'patricks_sort_woocommerce_available_shipping_methods', 10, 2 );

function patricks_sort_woocommerce_available_shipping_methods( $rates, $package ) {

	//  if there are no rates don't do anything

	if ( ! $rates ) {

		return;

	}

	

	// get an array of prices

	$prices = array();

	foreach( $rates as $rate ) {

		$prices[] = $rate->cost;

	}

	

	// use the prices to sort the rates

	array_multisort( $prices, $rates );

	

	// return the rates

	return $rates;

}

// code added for short shipping method by cost Emm 19.10.2010 End..





// coupon message international shipping 24 Nov 2014 EOF  



/*function coupon_usa(){



	global $woocommerce;

	$shipCountry = $woocommerce->customer->get_country();



	$shipState = WC()->customer->shipping_state; 

	$excluded_states = array( 'AK','HI' );



	if($shipCountry != 'US' || in_array($shipState, $excluded_states)){

	$woocommerce->cart->remove_coupons(); 

	}*/

	/*$woocommerce->clear_messages();

	$woocommerce->add_error('Test User');

	$woocommerce->show_messages();

}



add_action( 'woocommerce_shipping_init', 'coupon_usa' );*/

///check osc detail 02-Dec-2014 bof



/*remove_filter( 'authenticate', 'wp_authenticate_username_password', 20, 3 );



add_filter('authenticate', 'check_login',20, 3);

function check_login ($user, $username, $password) {

    global $wpdb;

	$user_email = strip_tags($_POST['username']);



	$password = strip_tags($_POST['password']);

	$get_wordpress_customer_sql = $wpdb->get_results("SELECT 

	oscomm_password,

	ID,

	u.user_login,

	meta_value 

		FROM `wp_users` as u JOIN `wp_usermeta` as m ON (u.ID = m.user_id) 



			WHERE 



				m.meta_key = 'osc_id' 



				AND  



				u.user_email = '".$user_email."' 



				AND 



				u.oscomm_password <> ''");	

	if(count($get_wordpress_customer_sql) > 0){

		$encrypted = $get_wordpress_customer_sql[0]->oscomm_password;

		if ($password != '' && $encrypted != '') {

			$stack = explode(':', $encrypted); 

			if (md5($stack[1] . $password) == $stack[0]) {



				$new_password =  wp_hash_password($password);



				$wpdb->query("UPDATE `wp_users` set user_pass = '".$new_password."',oscomm_password = '' where ID = '".$get_wordpress_customer_sql[0]->ID."'");		



			}



		}



	}



	return wp_authenticate_username_password( NULL, $get_wordpress_customer_sql[0]->user_login, $new_password );



}*/



///check osc detail 02-Dec-2014 eof







/////coupon add coupon in order confirmation mail bof



//add_action( 'woocommerce_email_after_order_table', 'add_payment_method_to_admin_new_order', 15, 2 );



 

/**



* Add used coupons to the order confirmation email



*



*/



/*function add_payment_method_to_admin_new_order( $order, $is_admin_email ) {



    if ( $is_admin_email ) {



        if( $order->get_used_coupons() ) {



            $coupons_count = count( $order->get_used_coupons() );



            echo '<h4>' . __('Coupons used') . ' (' . $coupons_count . ')</h4>';



            echo '<p><strong>' . __('Coupons used') . ':</strong> ';



            $i = 1;



            $coupons_list = '';



            foreach( $order->get_used_coupons() as $coupon) {



                $coupons_list .= $coupon;



                if( $i < $coupons_count )



                $coupons_list .= ', ';



                $i++;



            }



            echo '<p><strong>Coupons used (' . $coupons_count . ') :</strong> ' . $coupons_list . '</p>';



        } // endif get_used_coupons



    } // endif $is_admin_email



}*/



add_action( 'woocommerce_email_after_order_table', 'custom_checkout_field_display_admin_order_meta', 10, 2 );

 

/**



* Add used coupons to the order edit page



*



*/



function custom_checkout_field_display_admin_order_meta($order){



    if ( !$is_admin_email ) {



		if( $order->get_used_coupons() ) {



			$coupons_count = count( $order->get_used_coupons() );



			echo '<h4>' . __('Discounts Applied') . ' (' . $coupons_count . ')</h4>';



			echo '<p><strong>' . __('Discounts Applied') . ':</strong> ';



			$i = 1;



			foreach( $order->get_used_coupons() as $coupon) {



				echo $coupon;



				if( $i < $coupons_count )



				echo ', ';



				$i++;



			}



			echo '</p>';



		}



	}



}



//// coupon add coupon in order confirmation mail eof



add_action( 'woocommerce_after_cart', 'after_proced_to_checkout', 10, 2 );



function after_proced_to_checkout(){



	echo '<img src="'.get_stylesheet_directory_uri().'/credit-cards.jpg" style="float:right;margin-right:28px;">';

}



//22-Dec-2014 bof 



/*add_action( 'woocommerce_check_cart_items', 'flat_rate_shipping');



function flat_rate_shipping( $rates) {



	 if( is_cart() || is_checkout() ) {



        global $woocommerce;



		$cart_subtotal = WC()->cart->cart_contents_total;



		if($cart_subtotal<=6){



			if ( isset( $rates['flat_rate'] ) ) {



				$flat_rate = $rates['flat_rate'];



				$rates = array();



				$rates['flat_rate'] = $flat_rate;

			}



			wc_add_notice('test-if');



		} else {



			//unset( $rates['flat_rate'] );		



			wc_add_notice('test-else');



		}



		return $rates;

	}



}*/



//22-Dec-2014 eof



//17-07-2015 for display social media on footer bof 



 register_sidebar(

array(

		'name'=>'Footer-Social-Media',



		'before_widget'=>'',



		'after_widget'=>'',



		'before_title'=>'',



	  ));

//17-07-2015 Emm footer EOF.. 



//17-07-2015 for display social media on header bof 

 register_sidebar(

array(



		'name'=>'Header-Social-Media',



		'before_widget'=>'',



		'after_widget'=>'',



		'before_title'=>'',



	  ));



 //17-07-2015 Emm header EOF..



function custom_wc_ajax_variation_threshold( $qty, $product ) {



	return 100;



}

add_filter( 'woocommerce_ajax_variation_threshold', 'custom_wc_ajax_variation_threshold', 100, 2 );



/*add_filter( 'gform_field_validation_1_8', 'ups_address_validation', 10, 4 );

function tep_not_null($value) {

    if (is_array($value)) {

      if (sizeof($value) > 0) {

        return true;

      } else {

        return false;

      }

    } else {

      if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {

        return true;

      } else {

        return false;

      }

    }

  }

  



function ups_address_validation($result, $value, $form, $field){

     //address field will pass $value as an array with each of the elements as an item within the array, the key is the field id

     //if ( ! $result['is_valid'] && $result['message'] == 'This field is required. Please enter a complete address.' ) {

        if(1){ 

        //address failed validation because of a required item not being filled out

        //do custom validation

        $street  = rgpost( 'input_8' );//rgar( $value, $field->id . '_8' );

        $street2 = rgpost( 'input_9' );//rgar( $value, $field->id . '_9' );

        $city    = rgpost( 'input_10' );//rgar( $value, $field->id . '_10' );

        $state   = rgpost( 'input_7' );//rgar( $value, $field->id . '_7' );

        $zip     = rgpost( 'input_11' );//rgar( $value, $field->id . '_11' );

        //check to see if the values you care about are filled out

        if ( (empty( $street ) && empty( $street2 )) || empty( $city ) || empty( $state ) || empty($zip) ) {

            $result['is_valid'] = false;

            $result['message']  = 'This field is required. Please enter address line 1, city, and state.';

        } else {

            

            $res = call_usps_api_address_validate($street,$street2,$city,$state,$zip);

            $btrueFalse=false;

            if($res=='Verified')$btrueFalse=true;

            $result['is_valid'] = $btrueFalse;

            $result['message']  = $res;

        }

    }



    return $result;

}



function call_usps_api_address_validate($street,$street2,$city,$state,$zip){

    include_once('http_client.php');

       $request  = '<AddressValidateRequest USERID="830000001506"><IncludeOptionalElements>true</IncludeOptionalElements>

  <ReturnCarrierRoute>true</ReturnCarrierRoute><Address ID="0">';

       $dest_zip = substr($zip, 0, 5);

       $request .= '<Address1>' . $street . '</Address1>' .

                      '<Address2>' .$street2  . '</Address2>' .

                      '<City>'. $city .'</City>' .

                      '<State>' . $state .'</State>' .

                      '<Zip5>' . $dest_zip . '</Zip5>' .

                      '<Zip4></Zip4>' .

                      '</Address></AddressValidateRequest>';

            

                   

      $request = 'API=Verify&XML=' . urlencode($request);

      

      switch ('test') {

        case 'production': $usps_server = 'production.shippingapis.com';

                           $api_dll = 'shippingapi.dll';

                           break;

        case 'test':

        default:           $usps_server = 'testing.shippingapis.com';

                           $api_dll = 'shippingapitest.dll';

                           break;

      }



      $body = '';

      $http = new httpClient();

      if ($http->Connect($usps_server,80)) {

        $http->addHeader('Host', $usps_server);

        $http->addHeader('User-Agent', 'osCommerce');

        $http->addHeader('Connection', 'Close');

        $status = $http->Get('/' . $api_dll . '?' . $request);

  if ($status != 200) {

    die("Problem : " . $http->getStatusMessage());

  } else {

    $body = $http->getBody();

  }

  $http->Disconnect();

  // cut out the header

  

$body = substr($body, strpos($body, "<?xml"));

 //if (!(tep_session_is_registered('address_verification'))) tep_session_register('address_verification');

    if (preg_match('/<Error>/', $body)) {

            $description = preg_match('/<Description>(.*)<\/Description>/', $body, $regs);

            $description = $regs[1];

           // return array('error' => $description);

           $address_verification = 'Warning - Address verification Needed';

           $result = $address_verification;

    } else {

        $address_verification = 'Verified';

	   $result = $address_verification;

      }} else {

        $result = "Error: can not connect";

       

      }

      return $result;

}*/



//code deployed from live on 14.06.2016 for usps address verification

add_filter( 'gform_field_validation_1_8', 'ups_address_validation', 10, 4 );



function tep_not_null($value) {



    if (is_array($value)) {



      if (sizeof($value) > 0) {



        return true;



      } else {



        return false;



      }



    } else {



      if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {



        return true;



      } else {



        return false;



      }



    }



  }



  







function ups_address_validation($result, $value, $form, $field){



     //address field will pass $value as an array with each of the elements as an item within the array, the key is the field id



     //if ( ! $result['is_valid'] && $result['message'] == 'This field is required. Please enter a complete address.' ) {



        



        //address failed validation because of a required item not being filled out



        //do custom validation



        $street  = rgpost( 'input_8' );//rgar( $value, $field->id . '_8' );



        $street2 = rgpost( 'input_9' );//rgar( $value, $field->id . '_9' );



        $city    = rgpost( 'input_10' );//rgar( $value, $field->id . '_10' );



        $state   = rgpost( 'input_7' );//rgar( $value, $field->id . '_7' );



        $zip     = rgpost( 'input_11' );//rgar( $value, $field->id . '_11' );



        //check to see if the values you care about are filled out



        if ( (empty( $street ) && empty( $street2 )) || empty( $city ) || empty( $state ) || empty($zip) ) {



            $result['is_valid'] = false;



            //$result['message']  = 'This field is required. Please enter address line 1, city, and state.';



            $result['message']  = 'This field is required.';



        } else {



            $res = call_usps_api_address_validate($street,$street2,$city,$state,$zip);



            $btrueFalse=false;



            //if($res=='Verified')$btrueFalse=true;



            if($res=='Verified'){



                $result['is_valid'] = true;



            }else{



                if($res =='Please fill correct Zipcode' || $res =='Please enter 5 digit zip code only'){



                    add_filter( 'gform_field_validation_1_11', 'validate_zipcode', 10, 4 );



                    $result['is_valid'] = true;



                }

                



                if($res =='Please fill correct city'){



                    add_filter( 'gform_field_validation_1_10', 'validate_city', 10, 4 );



                    $result['is_valid'] = true;



                }



                if($res =='Please select correct state'){



                    add_filter( 'gform_field_validation_1_7', 'validate_state', 10, 4 );



                    $result['is_valid'] = true;



                }



                /*if($res='Please fill correct address line 2'){



                    add_filter( 'gform_field_validation_4_9', 'validate_address', 10, 4 );



                    $result['is_valid'] = true;



                }*/



                //if($res =='Warning - Address verification Needed'|| $res =='Please fill correct address line 1'){



                  if($res =='Warning - Address verification Needed'){



                    $result['is_valid'] = false;



                    $result['message'] = '';



                }



            }  



                



            /*$msg = '<script type="text/javascript"> jQuery(doucment).ready(function(){jQuery(".validation_error").append("<br>'. $res .'")});  </script>';*/



            //$result['message']  = $res;



        }



    return $result;



}







/*function validate_address($result, $value, $form, $field){



    $result['is_valid']= false;



    $result['message'] ="Please fill correct address line 2";



    return $result;



}*/



function validate_zipcode($result, $value, $form, $field){



    $result['is_valid']= false;

  if (strlen($value) > 5)

    $result['message'] = 'Please correct zip code, use 5 digit code only.';

    else

    $result['message'] ="Please fill correct Zipcode";



    return $result;



}





function validate_city($result, $value, $form, $field){



    $result['is_valid']= false;



    $result['message'] ="Please fill correct city";



    return $result;



}



function validate_state($result, $value, $form, $field){



    $result['is_valid']= false;



    $result['message'] ="Please select correct state";



    return $result;



}







function call_usps_api_address_validate($street,$street2,$city,$state,$zip){



    include_once('http_client.php');



      



       $request  = '<AddressValidateRequest USERID="830000001506"><IncludeOptionalElements>true</IncludeOptionalElements>



  <ReturnCarrierRoute>true</ReturnCarrierRoute><Address ID="0">';



       



	   $dest_zip='';



	   



       $zip4='';



       



	   if(strlen($zip)>=5){



       		$dest_zip = $zip;



       } else{



        	$zip4 = $zip;



       }



	   



       $request .= '<Address1>' . htmlspecialchars($street) . '</Address1>' .



                      '<Address2>' .htmlspecialchars($street2)  . '</Address2>' .



                      '<City>'. htmlspecialchars($city) .'</City>' .



                      '<State>' . htmlspecialchars($state) .'</State>' .



                      '<Zip5>' . htmlspecialchars($dest_zip) . '</Zip5>' .



                      '<Zip4>'. htmlspecialchars($zip4) .'</Zip4>' .



                      '</Address></AddressValidateRequest>';



                     



      $request = 'API=Verify&XML=' . urlencode($request);



      



      switch ('production') {



        case 'production': $usps_server = 'production.shippingapis.com';



                           $api_dll = 'shippingapi.dll';



                           break;



        case 'test':



        default:           $usps_server = 'testing.shippingapis.com';



                           $api_dll = 'shippingapitest.dll';



                           break;



      }







      $body = '';



      $http = new httpClient();



      if ($http->Connect($usps_server,80)) {



        $http->addHeader('Host', $usps_server);



        $http->addHeader('User-Agent', $_SERVER['HTTP_USER_AGENT']);



        $http->addHeader('Connection', 'Close');



        $status = $http->Get('/' . $api_dll . '?' . $request);



  if ($status != 200) {



    //die("Problem : " . $http->getStatusMessage());



    $result ="Not able to connect with usps server";



  } else {



    $body = $http->getBody();



  }



  $http->Disconnect();



  



  // cut out the header



//  echo $request;



  //echo '<br>';



  //die( $request .' '. $body);

if ($_GET['debug'] == '1') {

echo 'Request<br><pre>' . urldecode($request) . '<br>';

echo $body . '</pre>';

die;

}



$body = substr($body, strpos($body, "<?xml"));



 //if (!(tep_session_is_registered('address_verification'))) tep_session_register('address_verification');



    if (preg_match('/<Error>/', $body)) {



            $description = preg_match('/<Description>(.*)<\/Description>/', $body, $regs);



            $description = $regs[1];



           // return array('error' => $description);



           $address_verification = 'Warning - Address verification Needed';



           $result = $address_verification;



    } else {



            $city_val =preg_match('/<City>(.*)<\/City>/', $body, $regs);



            $city_val = $regs[1];



            /*$street_val='';



            if($street2!=''){



            $street_val =preg_match('/<Address1>(.*)<\/Address1>/', $body, $regs);



            $street_val = $regs[1];



            }



            $street2_val =preg_match('/<Address2>(.*)<\/Address2>/', $body, $regs);



            $street2_val = $regs[1];*/



            



            



            //$street2_val =preg_match('/<Address2>(.*)<\/Address2>/', $body, $regs);



            //$street2_val = $regs[1];



            $Zip_val = preg_match('/<Zip5>(.*)<\/Zip5>/', $body, $regs);



            $Zip_val = $regs[1];



            $Zip4_val = preg_match('/<Zip4>(.*)<\/Zip4>/', $body, $regs);



            $Zip4_val = $regs[1];



            $state_val =preg_match('/<State>(.*)<\/State>/', $body, $regs);



            $state_val = $regs[1];



            



            $result ="Verified";

       



            /*if(strtoupper($street)!= strtoupper($street2_val)){



                $result = "Please fill correct address line 1";    



            }elseif(strtoupper($street2)!= strtoupper($street_val) && $street2!=''){



                $result = "Please fill correct address line 2";



            }else*/



            if(strtoupper($city)!= strtoupper($city_val)){



                $result = "Please fill correct city";



            }elseif($dest_zip != '' && $dest_zip != $Zip_val){



                $result = "Please enter 5 digit zip code only";



            }elseif($zip4!='' && strtoupper($zip4)!= strtoupper($Zip4_val)){



                $result = "Please fill correct Zipcode";



            }elseif($state_val!= $state ){



                $result = "Please select correct state";



            }



        



      }} else {



        $result = "Error: can not connect";



       



      }



      return $result;



}







function getSateArray(){



    $state = array();



    $state['AL'] = 'Alabama';



    $state['AK'] = 'Alaska ';



    $state['AS'] = 'American Samoa';



    $state['AZ'] = 'Arizona';



    $state['AR'] = 'Arkansas';



    $state['CA'] = 'California ';



    $state['CO'] = 'Colorado';



    $state['CT'] = 'Connecticut';



    $state['DE'] = 'Delaware';



    $state['DC'] = 'Dist. of Columbia';



    $state['FL'] = 'Florida';



    $state['GA'] = 'Georgia';



    $state['GU'] = 'Guam';



    $state['HI'] = 'Hawaii';



    $state['ID'] ='Idaho';



    $state['IL'] ='Illinois';



    $state['IN'] = 'Indiana';



    $state['IA'] ='Iowa';



    $state['KS'] = 'Kansas';



    $state['KY'] ='Kentucky';



    $state['LA'] = 'Louisiana';



    $state['MD'] ='Maryland';



    $state['ME'] = 'Maine';



    $state['MH'] ='Marshall Islands';



    $state['MA'] ='Massachusetts';



    $state['MI'] ='Michigan';



    $state['FM'] ='Micronesia';



    



    $state['MN'] ='Minnesota';



    $state['MS'] = 'Mississippi';



    $state['MD'] ='Maryland';



    $state['MO'] = 'Missouri';



    $state['MT'] ='Montana';



    $state['NE'] ='Nebraska';



    $state['NV'] ='Nevada';



    $state['NH'] ='New Hampshire';



    



    $state['NJ'] ='New Jersey';



    $state['NM'] = 'New Mexico';



    $state['NY'] ='New York';



    $state['NC'] = 'North Carolina';



    $state['ND'] ='North Dakota';



    $state['MP'] ='Northern Marianas';



    $state['OH'] ='Ohio';



    $state['OK'] ='Oklahoma';



    



    $state['OR'] ='Oregon';



    $state['PW'] = 'Palau';



    $state['PA'] ='Pennsylvania';



    $state['PR'] = 'Puerto Rico';



    $state['RI'] ='Rhode Island';



    $state['SC'] ='South Carolina';



    $state['SD'] ='South Dakota';



    $state['TN'] ='Tennessee';



    



    $state['TX'] ='Texas';



    $state['UT'] = 'Utah';



    $state['VT'] ='Vermont';



    $state['VA'] = 'Virginia';



    $state['VI'] ='Virgin Islands';



    $state['WA'] ='Washington';



    $state['WV'] ='West Virginia';



    $state['WI'] ='Wisconsin';



    $state['WY'] ='Wyoming';



    



    return $state;

}

/**

 * Hide shipping rates when free shipping is available

 *

 * @param array $rates Array of rates found for the package

 * @param array $package The package array/object being shipped

 * @return array of modified rates

 */

 

 /* Emm 29-06-2016 Beg..

function hide_shipping_when_free_is_available( $rates, $package ) {

	$new_rates = array();



	foreach ( $rates as $rate_id => $rate ) {

		// Only modify rates if free_shipping is present.

		if ( 'legacy_free_shipping' === $rate->method_id ) {

			$new_rates[ $rate_id ] = $rate;

              unset( $rates['flat_rate:2'] );

			break;

		}

	}



	// To unset all methods except for free_shipping, do the following:

	if ( ! empty( $new_rates ) ) {

		return $new_rates;

	}



	return $rates;

}



add_filter( 'woocommerce_package_rates', 'hide_shipping_when_free_is_available', 10, 2 );

*/

// Emm 29/06/2016  End..



// added on 10-02-2017 to Store cart weight in the database #start

add_action('woocommerce_checkout_update_order_meta', 'woo_add_cart_weight');

function woo_add_cart_weight( $order_id ) {

    global $woocommerce;

    

    $weight = $woocommerce->cart->cart_contents_weight;

    update_post_meta( $order_id, '_cart_weight', $weight );

}
// added on 10-02-2017 to Store cart weight in the database #ends

// added on 15-03-2017 to unset address type after successfull checkout #starts
add_action('woocommerce_payment_complete', 'clear_custom_session', 10, 1);
function clear_custom_session($order_id) {
   unset( WC()->session->shipping_address_type);
}
// added on 15-03-2017 to unset address type after successfull checkout #ends

// START added for fix no new order mail issue. 10.04.2017
add_filter( 'woocommerce_defer_transactional_emails', '__return_false' );  //added for fix no new order mail issue
// END  10.04.2017
//Code added for pending order  Start 23.05.2017
function pending_order_email_notify($order_id) {
    global $woocommerce,$wpdb;
    $order = new WC_Order( $order_id );
    $status = $order->status;    
   $payment_method  =  $order->payment_method;
   $order_transaction = $order->transaction_id;
   //$order_transaction = $wpdb->get_var("SELECT pm.meta_value, pm.meta_key FROM wp_postmeta pm, wp_posts p WHERE p.ID = pm.post_id and p.ID = $order_id and pm.meta_key = '_transaction_id'");
   //$paypal_payment_status = $wpdb->get_var("SELECT pm.meta_value, pm.meta_key FROM wp_postmeta pm, wp_posts p WHERE p.ID = pm.post_id and p.ID = $order_id and pm.meta_key = '_paypal_status'");
   
   if($order_transaction=="" && $payment_method=="ppec_paypal" && $status=="pending"){      
        $to = "sunaina@focusindia.com";      
        $headers .= 'Content-Type: text/html'."\r\n";
        $headers .= 'charset=UTF-8'."\r\n";
        $headers .= 'From: Wikkistix <admin@wikkistix.com>' . "\r\n";
        $email_subject = "Pending order Notifications";	    
        //$message1 = "Order content is :".$ab." Id : ". $order_id.". order Status is :".$status." method : ".$payment_method." Payment status ".$payment_status." Transaction :".$order_transaction; 
        

$message = sprintf( __( 'You have received a new pending order from %s. Their order is as follows:', 'woocommerce' ), $order->get_formatted_billing_full_name() ) . "<br/><br/>";
$message .=  "**********************************************************************<br/><br/>";
$message .= "Order number: ".$order->order_key. "<br/>";
$message .= "Subtotal: ".$order->shipping_total . "<br/>";
$message .= "Shipping: ".$order->shipping_total . "<br/>";
$message .= "AZ Tax: ".$order->shipping_total . "<br/>";
$message .= "Payment method: ".$order->shipping_total . "<br/>";
$message .= "Total: ".$order->shipping_total . "<br/>";


$message .=  'Credit Card Type :-' . "\t " . get_post_meta( $order_id, '_wc_authorize_net_aim_card_type',true ) . "<br/>";
$message .=  "<br/>****************************************************<br/><br/>";
$message .=  "<br/><br/>";
$message .=  __( 'Customer details', 'woocommerce' ) . "<br/>";
if ( $order->billing_email )
 $message .=  __( 'Email:', 'woocommerce' ); echo $order->billing->billing_email . "<br/>";
if ( $order->billing_phone )
$message .=  __( 'Tel:', 'woocommerce' ); ?> <?php echo $order->billing->billing_phone . "<br/>";
$message .= "Billing Address:<br/>".$order->billing->first_name." ".$order->billing->last_name."<br/>".$order->billing->address_1." ".$order->billing->address_2."<br/>".$order->billing->city." ".$order->billing->state." ".$order->billing->zip_codes."<br/><br/>";
$message .= "Shipping Address:<br/>".$order->shipping->first_name." ".$order->shipping->last_name."<br/>".$order->shipping->address_1." ".$order->shipping->address_2."<br/>".$order->shipping->city." ".$order->shipping->state." ".$order->shipping->zip_codes."<br/><br/>";
$message .=  "<br/>****************************************************<br/><br/>";
$message .=  apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );

        //wp_mail($to, $email_subject, $message, $headers);
     }
}
add_action( 'woocommerce_new_order', 'pending_order_email_notify',  10, 1  );
// Code Added for pending order End 23.05.2017


/**
 * Change the default country on the checkout for non-existing users only
 */
add_filter( 'default_checkout_billing_country', 'change_default_checkout_billing_country', 10, 1 );

function change_default_checkout_billing_country( $country ) {
    // If the user already exists, don't override country
    if ( WC()->customer->get_is_paying_customer() ) {
        return $country;
    }

    return 'US'; // Override default to United state (an example)
}

add_filter( 'default_checkout_shipping_country', 'change_default_checkout_shipping_country', 10, 1 );

function change_default_checkout_shipping_country( $country ) {
    // If the user already exists, don't override country
    if ( WC()->customer->get_is_paying_customer() ) {
        return $country;
    }

    return 'US'; // Override default to United state (an example)
}

add_action('woocommerce_add_to_cart' , 'set_country_befor_cart_page'); 

function set_country_befor_cart_page(){
    
    
    if ( WC()->customer->get_is_paying_customer() ) {
        //WC()->customer->set_country(''); //reset default country
        //WC()->customer->set_shipping_country('');
    }
    else{
         WC()->customer->set_country('US'); //set country code of default country
         WC()->customer->set_shipping_country('US');
    }
}

function hide_shipping_methods( $available_shipping_methods, $package ) {
    global $woocommerce;
    // get cart contents
    $cart_items = $woocommerce->cart->get_cart();
    
    $subscription = false;
    foreach ( $cart_items as $key => $item ) {
        $product = wc_get_product($item['product_id']);
       if($product->get_type() == 'subscription') {
         $subscription = true;
       } 
    } 
   // $nextPayment = $wc_subscription->get_time('next_payment');
  //  $dt = new DateTime("@$nextPayment");
  //  echo $dt->format('Y-m-d H:i:s');
    foreach( $available_shipping_methods as $method => $method_obj ) {
        if ($subscription && $method_obj->method_id != 'flat_rate') {
                unset( $available_shipping_methods[$method] );
        } elseif (!$subscription && $method_obj->method_id == 'flat_rate') {
             unset( $available_shipping_methods[$method] ); 
        } 
    }
    return $available_shipping_methods;
}
add_filter( 'woocommerce_package_rates', 'hide_shipping_methods', 10, 2 );

function sync_new_renewal_date($subscription, $order, $recurring_cart) {
    $start_months = array('1','3','5','9','11');
    $month = date("n");
    $year = date("Y");
    $get_month=false;
    While(!$get_month) {
        if (in_array($month, $start_months)) {
            $get_month=true;
        } else {
          if ($month >= 12) {
            $month = 1;
            $year += 1;
          } else { 
            $month += 1;
           } 
        }
    }
   $next_payment =  date('Y-m-d H:i:s', mktime(12, 0, 0, $month, 1, $year));
    // Available keys include: 'start', 'trial_end', 'next_payment', 'last_payment' or 'end'
    $new_dates = array(
        'next_payment' => $next_payment
    );
    $subscription->update_dates($new_dates);
}
add_action('woocommerce_checkout_subscription_created', 'sync_new_renewal_date', 10, 3);

function wcs_add_cart_first_renewal_payment_date_custom( $order_total_html, $cart ) {
 if ( 0 !== $cart->next_payment_date ) {
     $start_months = array('1','3','5','9','11');
     $month = date("n");
     $year = date("Y");
     $get_month=false;
     While(!$get_month) {
        if (in_array($month, $start_months)) {
            $get_month=true;
        } else {
          if ($month >= 12) {
            $month = 1;
            $year += 1;
          } else { 
            $month += 1;
           } 
        }
    }  
    $next_payment =  date('Y-m-d H:i:s', mktime(12, 0, 0, $month, 1, $year));   
    $first_renewal_date = date_i18n( wc_date_format(), wcs_date_to_time( get_date_from_gmt($next_payment) ) );
            // translators: placeholder is a date
    $order_total_html  .= '<div class="first-payment-date"><small>' . sprintf( __( 'First renewal: %s', 'woocommerce-subscriptions' ),$first_renewal_date ) .  '</small></div>';
  }

        return $order_total_html;
    }
//add_filter('wcs_add_cart_first_renewal_payment_date', 'custom_renewal_date', 10, 2 );

remove_filter( 'wcs_cart_totals_order_total_html', 'wcs_add_cart_first_renewal_payment_date', 10, 2 );
add_filter( 'wcs_cart_totals_order_total_html', 'wcs_add_cart_first_renewal_payment_date_custom', 10, 2 ); 

//add_filter( 'woocommerce_cart_calculate_fees', 'add_recurring_postage_fees', 10, 1 );
    function add_recurring_postage_fees( $cart ) {
     if ( ! empty( $cart->recurring_cart_key ) ) {
       remove_action( 'woocommerce_cart_totals_after_order_total', array( 'WC_Subscriptions_Cart', 'display_recurring_totals' ), 10 );
       remove_action( 'woocommerce_review_order_after_order_total', array( 'WC_Subscriptions_Cart', 'display_recurring_totals' ), 10 );
     }
    }

