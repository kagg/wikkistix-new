<?php
/**
 * Customer new account email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails/Plain
 * @version     2.5.3
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo $email_heading . "!<br/><br/>";

echo sprintf( __( "Thanks for creating an account on %s. <br/><br/> Your username is <strong>%s</strong>.", 'woocommerce' ), $blogname, $user_login ) . "<br/><br/>";

if ( get_option( 'woocommerce_registration_generate_password' ) === 'yes' && $password_generated )
	echo sprintf( __( "Your password is <strong>%s</strong>.", 'woocommerce' ), $user_pass ) . "<br/><br/>";

echo sprintf( __( 'You can access your account area to view your orders and change your password here: %s.', 'woocommerce' ), get_permalink( wc_get_page_id( 'myaccount' ) ) ) ;

//echo "\n****************************************************\n\n";
echo "<br/><br/>Thank you! <br/>";

echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) );
echo "<br/>The Wikki Stix Co.";