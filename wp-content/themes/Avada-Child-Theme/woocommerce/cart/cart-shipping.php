<?php
/**
 * Shipping Methods Display
 *
 * In 2.1 we show methods per package. This allows for multiple methods per order if so desired.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php 

//* 27-04-2015 BOf emm

if(is_checkout()){

	$th_colspan = $td_colspan = '';} 

	else {	$th_colspan = 'colspan="3"';

  $td_colspan = 'colspan="2"';

}

//* 27-04-2015 EOf emm

?>
<tr class="shipping">
	<th <?php echo $th_colspan; ?>><?php // commented by emm echo wp_kses_post( $package_name );     
    //added byu emm start
    if ( $show_package_details ) {

			printf( __( 'Shipping #%d', 'woocommerce' ), $index + 1 );

		} else {

			_e( 'Shipping and Handling', 'woocommerce' );

		}
    //added by emm end
	?>
    </th>
	<td <?php echo $td_colspan; ?> data-title="<?php echo esc_attr( $package_name ); ?>">      
		<?php if ( 1 < count( $available_methods ) ) : ?>
        <!-- Added By emm Start -->
        <?php   if (!array_key_exists('flat_rate:1', $available_methods)) {?> 
                            <style type="text/css">img.flat_rate_img { display: none; } </style>
                    <?php } ?>
        <!-- Added By emm End -->
			<ul id="shipping_method" style="width:300px;"> <!-- Modified by emm -->
				<?php 
                $disable = '';
                foreach ( WC()->cart->get_coupons( 'cart' ) as $code => $coupon ) :
                $data = $coupon->get_data();
                if($data['free_shipping']==true){
                    $disable='disabled';
                    break;
                }
                endforeach;
                foreach ( $available_methods as $method ) : ?>
                    <?php if( $method->id == 'flat_rate:1'){  $font_colors = 'class="flat_emm" style="color:#43ab05;font-weight: bold;"';  } else $font_colors =''; ?>
					<li>
						<?php
							printf( '<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s  '.$disable.'/>
                <label for="shipping_method_%1$d_%2$s" '.$font_colors.'.>%5$s</label>',                             
								$index, sanitize_title( $method->id ), esc_attr( $method->id ), checked( $method->id, $chosen_method, false ), wc_cart_totals_shipping_method_label( $method ) );
                                
                            if( $method->id == 'flat_rate:1'){ //added by emm start?>                      

                    <div>If you're not in a hurry, take advantage of our new flat rate shipping offer! Please note: delivery can take 7-14 business days and there is only limited tracking information available.</div>                                                

                         <?php }  //added by emm end
							do_action( 'woocommerce_after_shipping_rate', $method, $index );
						?>
					</li>
                    <li>&nbsp;</li>
				<?php endforeach; ?>
                <li><input type="checkbox" style="visibility: hidden;" /> <label>Please note: All orders take one day for processing, so add a day to all service times listed.</label></li>
			</ul>
		<?php elseif ( 1 === count( $available_methods ) ) :  ?>
			<?php
				$method = current( $available_methods );
				printf( '%3$s <input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d" value="%2$s" class="shipping_method" />', $index, esc_attr( $method->id ), wc_cart_totals_shipping_method_label( $method ) );
				do_action( 'woocommerce_after_shipping_rate', $method, $index );
			?>
           <?php   if (!array_key_exists('flat_rate:1', $available_methods)) {?> 
                            <style type="text/css">img.flat_rate_img { display: none; } </style>
                    <?php } ?>
           
		<?php elseif ( WC()->customer->has_calculated_shipping() ) : ?>            
			<?php echo apply_filters( is_cart() ? 'woocommerce_cart_no_shipping_available_html' : 'woocommerce_no_shipping_available_html', wpautop( __( 'There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'woocommerce' ) ) ); ?>
            <style type="text/css">img.flat_rate_img { display: none; } </style>
		<?php elseif ( ! is_cart() ) : ?>
			<?php // echo wpautop( __( 'Enter your full address to see shipping costs.', 'woocommerce' ) ); //commented by emm ?>
            <?php echo wpautop( __( 'Please fill in your details to see available shipping methods.', 'woocommerce' ) ); //added by emm ?>            
            <style type="text/css">img.flat_rate_img { display: none; } </style>
            
        <?php //added by emm start
          elseif ( is_cart() ) : ?>

				<p><?php _e( 'Please continue to the checkout and enter your full address to see the available shipping options.', 'woocommerce' ); ?></p>
          <style type="text/css">img.flat_rate_img { display: none; } </style>
        <?php else : ?>

                
				<p><?php _e( 'Please fill in your details to see available shipping methods.', 'woocommerce' ); ?></p>
   
                <style type="text/css">img.flat_rate_img { display: none; } </style>
                
		<?php  //added by emm end
              endif; ?>

		<?php if ( $show_package_details ) : ?>
			<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html( $package_details ) . '</small></p>'; ?>
		<?php endif; ?>

		<?php if ( ! empty( $show_shipping_calculator ) ) : ?>
			<?php woocommerce_shipping_calculator(); ?>
		<?php endif; ?>
	</td>
</tr>
