<?php

/**

 * Checkout Form

 *

 * @author 		WooThemes

 * @package 	WooCommerce/Templates

 * @version     2.3.0

 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wc_print_notices();

/*if(!is_user_logged_in()){

	echo $info_message  = _e( 'Checkout as guest', 'woocommerce' ).' <label for="guestaccount" class="checkbox"> <input class="input-checkbox" id="guestaccount" type="checkbox" name="guestaccount" value="1" /> </label>';

}*/

?>

<?php

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout

if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {

	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );

	return;

}

// filter hook for include new pages inside the payment method

$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<form name="checkout" method="post" class="checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">

			<div class="col-1">

				<?php do_action( 'woocommerce_checkout_billing' ); 

				if(!is_user_logged_in()){

					echo $info_message  = '<input class="input-checkbox" id="guestaccount" type="checkbox" name="guestaccount" value="1" /><label for="guestaccount" class="checkbox"> Checkout as guest</label>'; 

				}

				?> 		

			</div>

			<div class="col-2">

				<?php do_action( 'woocommerce_checkout_shipping' ); ?>

			</div>

		</div>	

		

		<div style="clear: both;"></div>

		<div class="woocommerce-content-box full-width" id="order_note">
<p><strong><span style="color: #f00">BILLING ADDRESS:</span></strong> The billing address must match the billing address of the cardholder. </p>

			<p><strong>PLEASE NOTE:</strong> All orders take 1 business day to process. Please keep this in mind when selecting expedited shipping and add an additional business day when calculating delivery needs. Example: <strong>2 Day Air</strong> (one business day to process the order and two business days to deliver). </p>

			<p><strong>INTERNATIONAL ORDER?</strong> If you select <strong>FedEx International Economy</strong> or <strong>International Priority</strong>, customs clearance fees will be included in your shipping and handling charge. HOWEVER, any GST or other national taxes may be assessed upon delivery.</p>	

		</div>

		<div style="clear: both;"></div>

	

         <!--

		<div style="clear: both;"></div>

		<div class="woocommerce-content-box full-width" id="order_note">

<p><strong style="color: #f00">CREDIT CARD ORDERS: </strong> We have been experiencing periodic hosting issues...if you have a problem processing your credit card order, please contact us at 800-869-4554 or 602-870-9937, we'll be happy to assist! We apologize for any inconvenience!</p>	

		</div>

		-->

		<div style="clear: both;"></div>



		

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?> 

		<h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

	<?php endif; ?>

	<?php   do_action( 'woocommerce_checkout_order_review' ); ?>

</form>



<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

<script>

	jQuery('document').ready(function(){

	

		jQuery("#createaccount").change(function(){

			if(jQuery("#createaccount").attr('checked'))
			{

				console.log('createaccount');				

				jQuery('#guestaccount').prop( "checked", false );    

			}  

		});

		

		jQuery("#guestaccount").change(function(){

			if(jQuery("#guestaccount").attr('checked'))
			{

				console.log('guestaccount');

				jQuery('#createaccount').prop( "checked", false );

				jQuery('div.create-account').slideUp();

			}	 

		});		

	});

</script>

<style>

	#guestaccount {

    margin-right: 7px;

}

</style>

<!--Emm 22-12-2015 To display ajax_loader.gif image on loading checkout page  beg..-->

<?php if(is_checkout()){ ?>	

<script type="text/javascript">

jQuery(window).load(function() {


		jQuery(".loader").delay(5000).fadeOut("slow");

})

</script>

<div class="loader"></div>

<style>

.loader {

	position: fixed;

	left: 0px;

	top: 0px;

	opacity: 0.6;

	width: 100%;

	height: 100%;

	z-index: 999999;

	background: #fff url('https://www.wikkistix.com/wp-content/themes/Avada-Child-Theme/ajax-loader.gif') 50% 50% no-repeat;

}

 .avada-myaccount-user{display:none !important;}  	

</style>

<?php } ?>

<!--Emm 22-12-2015 To display ajax_loader.gif image on loading checkout page  End..-->